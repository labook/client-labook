# Documentacion del proyecto Labook
Integrantes del proyecto:
- Ybarra Jonathan (FrontEnd)
- Federico Pagliari (BackEnd)
- Federico Marino (Product Manager)
- Gerardo Gonzalez (Producto Owner)
- Carlos Lombardi (Asesor de producción)

# Tecnologias utilizadas
En el proyecto Labook (Cuaderno Bitacora) utilizamos el stack P.E.R.N (Postgres, Express, Reactjs && Nodejs) 

| FrontEnd | BackEnd |
| ------ | ------ |
| React | Express |
| Redux | Node |
| Axios | Postgress |
| Materialize.css | Sequelize (ORM) |
| Hooks | JWT (Json Web token) |
| Functional Components | YUP (Validacion Schemas) |
| Sweet Alert (Modales) | Multer (Almacenamiento de PDF) |

### Instalaciones y arranque
Lo primero que debemos hacer en Backend y FrontEnd es correr el comando para instalar las dependencias.
```sh
$ npm install
```
Segundo a realizar en Backend, es correr las migraciones, para reflejar los modelos definidos en el servidor a la base de datos.
```sh
$ cd src
$ npx sequelize db:migrate
```
Por ultimo el script para levantar el servidor y el cliente.
```sh
$ npm start
```
# Funcionalidades alcanzadas FrontEnd / Backend
Registro de usuarios [Backend]
Inicio de sesion de un usuario
Manejo de Roles
Rol docente:
- Ver las materias y las comisiones a las que pertenece
- CRUD de el catalogo de recursos
- CRUD de una investigación, cargar un PDF 
- CRUD de un grupo, asignar estudiantes y una investigacion
- Ver el diseño experimental de cada grupo

Rol estudiante: 
- Ver los grupos a los que pertenece y correspondiente investigación
- Seleccionar los recursos del catalogo para una investigación
- Realizar el CRUD de registro de pasos para el diseño experimental

Rol Administrador:
- CRUD materias y comisiones
- Mismas funcionalidades que el rol docente


### Funcionalidades a mejorar

* Mejorar validaciones en servidor(actualmente solo se valida la existencia de un registro)
* Renovar token para usuarios logueados
* Implementar servicio externo de almacenamiento para PDF/imagenes(PDF, actualmente se almacena en el servidor/Imagenes, Base64)
* Realizar test
* Pantalla para registro de usuarios
* Cambiar modal para crear una materia por un modal de SweetAlert
* Cuando se crea una comision, formatear horarios y fechas
* Cuando se crea una investigacion, recibir la fecha de entrega
* Que el docente pueda checkear los pasos que el alumno carga en el diseño experimental (actualmente solo puede verlos)
* Mejorar el manejo de cantidad del catalago de recursos
* En el modelo de la comision, cambiar los dias por un array (actualmente es un string/trae problemas al cliente)
* Agregar paginacion/scroll infinito al catalogo de recursos
* Tomar los datos del usuario logueado y renderizarlos en el sidebar

# Breve explicacion de lo que se puede realizar

> Un usuario con rol administrador es capas de crear materias y asignarle una comision.
> A esta misma puede asignarle docentes, los cuales estaran a cargo de cada comision.
> Una vez el administrador asigna docentes a las diferentes comisiones, estos podran ver
> a cuales pertenecen y empezar a cargar investigaciones para los diferentes grupos que podran crear,
> y asignar diferentes estudiantes.
> Ahora toca que un alumno se loguee y pueda ver las investigaciones que el docente de esa comision le asigno.
> El alumno podra ver el diseño expiremental, en el cual tendra que hacer una seleccion de recursos del catalago, que
> tanto el docente como el administrador son los encargados del mantenimiento de este.
> Tambien deberan realizar un registro de pasos, donde documentaran las tareas que van realizando en el proyecto,
> esto es escencial ya que el docente podra ver lo que los alumnos van documentando.