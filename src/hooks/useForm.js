import { useState } from "react";

export const useForm = (initialState = {}) => {
    const [values, setValues] = useState(initialState);


    const setStateValues = (newValues) => {
        setValues(newValues);
    }

    const reset = () => {
        setValues(initialState);
    }

    const handleInputChange = ({ target }) => {
        setValues({
            ...values,
            [target.name]: target.value
        });
    }

    const handleCheckedChange = ({ target }) => {
        setValues({
            ...values,
            [target.name]: target.checked
        });
    }

    const handleInputChangeFile = async ({ target }) => {
        const file = target.files[0];
        if (file) {
            const fileTo64 = await convertBase64(file, target);
            setValues({
                ...values,
                [target.name]: fileTo64
            });
        }
    }

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = () => {
                resolve(reader.result);
            };

            reader.onerror = (error) => {
                reject(error);
            }
        });
    }

    return [values, handleInputChange, reset, handleCheckedChange, handleInputChangeFile, setStateValues];
}