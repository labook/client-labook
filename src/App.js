import React, { Component } from "react";
import "./App.css";
// Materialize
import "materialize-css/dist/css/materialize.min.css";
import { materializeJs } from "./utils/MaterializeJS/Materialize.service";
import './utils/MaterializeJS/materialize.css';
// Redux
import { Provider } from "react-redux";
import store from "./redux/store";

// React Router
import { Switch } from 'react-router-dom';
import routes from "./utils/config/routes";
import { RouteWithSubRoutes } from "./utils/config/RouteWithSubRoutes";

export default class App extends Component {
  componentDidMount() {
    materializeJs.sideNav();
    materializeJs.collapsible();
  }

  render() {
    return (
      <Provider store={store}>
        <Switch>
          {
            routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)
          }
        </Switch>
      </Provider>
    );
  }
}
