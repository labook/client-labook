import axios from "axios";
import { REACT_APP_APILOCAL } from "./urls";
import { isAuthenticated } from './auth/authService';

export const getStudentResearch = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/studentResearchs/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}