import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

export const getResearchsService = async (idStudent) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/researchs`,
        {
            params: { id: idStudent },
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const getResearchService = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/research/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const createResearchService = async ({ name, deliver_date, description }) => {
    const { token } = isAuthenticated();
    const res = await axios.post(`${REACT_APP_APILOCAL}/add-research`, {
        name,
        description,
        deliver_date: '2020-07-14T00:00:00.000Z'
    },
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const deleteResearchService = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-research/${id}`,
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const updateResearchService = async (id, { name }) => {
    const { token } = isAuthenticated();
    const res = await axios.put(`${REACT_APP_APILOCAL}/update-research/${id}`, {
        name
    },
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

// Group Researchs
export const getGroupResearchService = async (idGroup) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/groupResearchs`,
        {
            params: { id_group: idGroup },
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const getGroupResearIDchService = async (idGroup) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/groupResearch/${idGroup}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}


// StepResearches
export const createStepResearchService = async ({ step_number, step_description, teacher_check, id_group_research_fk }) => {
    const res = await axios.post(`${REACT_APP_APILOCAL}/add-stepResearch`, {
        step_number,
        step_description,
        teacher_check,
        id_group_research_fk
    }, {
        headers: {
            Authorization: 123
        }
    });
    return res;
}

export const deleteStepResearchService = async (id) => {
    const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-stepResearch/${id}`);
    return res;
}