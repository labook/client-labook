import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

class EquipmentService {

    async getEquipmentService(page_number, page_size) {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/equipments?page_number=${page_number}&&page_size=${page_size}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async getEquipmentIDService(id) {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/equipment/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async removeEquipmentService(id) {
        const { token } = isAuthenticated();
        const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-equipment/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async postEquipmentService(newEquipment) {
        const { token } = isAuthenticated();
        const res = await axios.post(`${REACT_APP_APILOCAL}/add-equipment`, {
            "name": newEquipment.name,
            "image": newEquipment.image,
            "description": newEquipment.description,
            "cant": newEquipment.cant
        }, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async updateEquipmentService(id, newReagent) {
        const { token } = isAuthenticated();
        const res = await axios.put(`${REACT_APP_APILOCAL}/update-equipment/${id}`, {
            "name": newReagent.name,
            "image": newReagent.image,
            "description": newReagent.description
        }, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async addEquipmentSelectionService(id_equipment_fk, id_group_research_fk, cant) {
        const { token } = isAuthenticated();
        const res = await axios.post(`${REACT_APP_APILOCAL}/add-equipmentSelection`, {
            id_equipment_fk,
            id_group_research_fk,
            cant
        }, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async getEquipmentsSelectionService() {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/equipmentSelections`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async deleteEquipmentsSelectionService(id) {
        const { token } = isAuthenticated();
        const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-equipmentSelection/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async updateEquipmentCantService(id, cant) {
        const { token } = isAuthenticated();
        const res = await axios.put(`${REACT_APP_APILOCAL}/update-resource/${id}/equipment`, {
            cant
            ,
            headers: {
                Authorization: token
            }
        });
        return res;
    }
}

let equipmentService = new EquipmentService();
export { equipmentService }
