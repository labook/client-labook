import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

class MaterialService {

    async getMaterialService(page_number, page_size) {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/materials?page_number=${page_number}&&page_size=${page_size}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async getMaterialIDService(id) {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/material/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async removeMaterialService(id) {
        const { token } = isAuthenticated();
        const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-material/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async postMaterialService({ name, image, description, cant }) {
        const { token } = isAuthenticated();
        const res = await axios.post(`${REACT_APP_APILOCAL}/add-material`, {
            "name": name,
            "image": image,
            "description": description,
            "cant": cant
        }, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async updateMaterialService(id, { name, image, description }) {
        const { token } = isAuthenticated();
        const res = await axios.put(`${REACT_APP_APILOCAL}/update-material/${id}`,
            {
                "name": name,
                "image": image,
                "description": description
            },
            {
                headers: {
                    Authorization: token
                }
            });
        return res
    }
    async addMaterialSelectionService(id_material_fk, id_group_research_fk, cant) {
        const { token } = isAuthenticated();
        const res = await axios.post(`${REACT_APP_APILOCAL}/add-materialSelection`, {
            id_material_fk,
            id_group_research_fk,
            cant
        }, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async getMaterialsSelectionService() {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/materialsSelection`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async deleteMaterialSelectionService(id) {
        const { token } = isAuthenticated();
        const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-materialSelection/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async updateMaterialCantService(id, cant) {
        const { token } = isAuthenticated();
        const res = await axios.put(`${REACT_APP_APILOCAL}/update-resource/${id}/material`, {
            cant
            ,
            headers: {
                Authorization: token
            }
        });
        return res;
    }
}

let materialService = new MaterialService();
export { materialService }