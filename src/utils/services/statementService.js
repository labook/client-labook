import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

export const getStatementService = async (idResearch) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/statements?id_research=${idResearch}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const addStatementPdfService = async (file, idResearch) => {
    const { token } = isAuthenticated();

    const formData = new FormData();

    formData.append('pdf', file.file);
    formData.append('id_research_fk', idResearch);

    const res = await axios.post(`${REACT_APP_APILOCAL}/add-statement`, formData, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const removeStatementService = async (idStatement) => {
    const { token } = isAuthenticated();

    const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-statement/${idStatement}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

