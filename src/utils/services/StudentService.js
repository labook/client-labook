import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

export const getStudentService = async (id_comission) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/student-status`,
        {
            params: { id_comission },
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const getStudentByIdService = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/student/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}
