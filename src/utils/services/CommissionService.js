import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

export const getCommissions = async () => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/comissions`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const getCommissionsByteacher = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/comissions?id_teacher=${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const getCommissionService = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/comission/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const postCommissions = async ({
    description,
    start_time,
    end_time,
    days,
    id_subject_fk,
    start_date,
    end_date,
    active
}) => {
    const { token } = isAuthenticated();
    const res = await axios.post(`${REACT_APP_APILOCAL}/add-comission`, {
        description,
        start_time,
        end_time,
        days,
        id_subject_fk,
        start_date,
        end_date,
        active
    }, {
        headers: {
            Authorization: token
        }
    });
    return res
}

export const putCommissions = async (id, {
    description,
    start_time,
    end_time,
    days,
    id_subject_fk,
    start_date,
    end_date,
    active
}) => {
    const { token } = isAuthenticated();
    const res = await axios.put(`${REACT_APP_APILOCAL}/update-comission/${id}`, {
        description,
        start_time,
        end_time,
        days,
        id_subject_fk,
        start_date,
        end_date,
        active
    }, {
        headers: {
            Authorization: token
        }
    });
    return res
}

export const removeCommission = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-comission/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const createTeacheComissionservice = async (id_teacher_fk, id_comission_fk) => {
    const { token } = isAuthenticated();
    const res = await axios.post(`${REACT_APP_APILOCAL}/add-teacherComission`, {
        id_teacher_fk,
        id_comission_fk
    }, {
        headers: {
            Authorization: token
        }
    });
    return res;
}