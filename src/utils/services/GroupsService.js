import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

export const getGroupsService = async (id_comission) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/groups`,
        {
            params: { id_comission },
            headers: {
                Authorization: token
            }
        },

    );
    return res;
}

export const getGroupService = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/group/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const postGroupsService = async ({ name, id_comission_fk }) => {
    const { token } = isAuthenticated();
    const res = await axios.post(`${REACT_APP_APILOCAL}/add-group`,
        {
            name,
            id_comission_fk
        },
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const postGroupResearchService = async (idGroup, idResearch) => {
    const { token } = isAuthenticated();
    const res = await axios.post(`${REACT_APP_APILOCAL}/add-groupResearch`,
        {
            id_group_fk: idGroup,
            id_research_fk: idResearch
        },
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const updateGroupService = async (id, { name, id_research_fk, id_comission_fk }) => {
    const { token } = isAuthenticated();
    const res = await axios.put(`${REACT_APP_APILOCAL}/upload-group/${id}`,
        {
            name,
            id_research_fk,
            id_comission_fk
        },
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const deleteGroupService = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-group/${id}`,
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}
