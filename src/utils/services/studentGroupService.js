import axios from "axios";
import { REACT_APP_APILOCAL } from "./urls";
import { isAuthenticated } from './auth/authService';

export const getStudentGroup = async () => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/studentGroups`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const getStudentGroupId = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/studentGroup/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const postStudentGroup = async (id_student_fk, id_group_fk) => {
    const { token } = isAuthenticated();
    const res = await axios.post(`${REACT_APP_APILOCAL}/add-studentGroup`,
        {
            id_student_fk,
            id_group_fk
        },
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}

export const deleteStudentGroupService = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-studentGroup/${id}`,
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res;
}