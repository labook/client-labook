import axios from 'axios';
import { REACT_APP_APILOCAL } from '../urls';

export const signinService = async ({ dni, password }) => {
    const res = await axios.post(`${REACT_APP_APILOCAL}/signIn`, {
        dni,
        password
    });
    return res;
}

export const signoutService = () => {
    if (typeof window !== "undefined") localStorage.removeItem('jwt');
}

export const authenticateService = (jwt) => {
    if (typeof window !== "undefined") {
        localStorage.setItem('jwt', JSON.stringify(jwt));
    }
}

export const isAuthenticated = () => {
    if (typeof window === "undefined") return false;

    if (localStorage.getItem('jwt')) {
        return JSON.parse(localStorage.getItem('jwt'));
    } else {
        return false;
    }
}

