import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

class ReagentService {

    async getReagentService(page_number, page_size) {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/reagents?page_number=${page_number}&&page_size=${page_size}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }

    async getReagentIDService(id) {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/reagent/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }

    async removeReagentService(id) {
        const { token } = isAuthenticated();
        const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-reagent/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async postReagentService({ name, cas, image, description, cant }) {
        const { token } = isAuthenticated();
        const res = await axios.post(`${REACT_APP_APILOCAL}/add-reagent`, {
            "name": name,
            "cas": Number(cas),
            "image": image,
            "description": description,
            "cant": cant
        }, {
            headers: {
                Authorization: token
            }
        });
        return res
    }

    async updateReagentService(id, { name, cas, image, description }) {
        const { token } = isAuthenticated();
        const res = await axios.put(`${REACT_APP_APILOCAL}/update-reagent/${id}`, {
            "name": name,
            "cas": Number(cas),
            "image": image,
            "description": description
        }, {
            headers: {
                Authorization: token
            }
        });
        return res
    }
    async addReagentSelectionService(id_reagent_fk, id_group_research_fk, cant) {
        const { token } = isAuthenticated();
        const res = await axios.post(`${REACT_APP_APILOCAL}/add-reagentSelection`, {
            id_reagent_fk,
            id_group_research_fk,
            cant
        }, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async getReagentsSelectionService() {
        const { token } = isAuthenticated();
        const res = await axios.get(`${REACT_APP_APILOCAL}/reagentsSelection`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async deleteReagentSelectionService(id) {
        const { token } = isAuthenticated();
        const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-reagentSelection/${id}`, {
            headers: {
                Authorization: token
            }
        });
        return res;
    }
    async updateReagentCantService(id, cant) {
        const { token } = isAuthenticated();
        const res = await axios.put(`${REACT_APP_APILOCAL}/update-resource/${id}/reagent`, {
            cant
            ,
            headers: {
                Authorization: token
            }
        });
        return res;
    }
}

let reagentService = new ReagentService();
export { reagentService }
