import axios from 'axios';
import { REACT_APP_APILOCAL } from './urls';
import { isAuthenticated } from './auth/authService';

export const getSubjects = async () => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/subjects`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const getSubject = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`${REACT_APP_APILOCAL}/subject/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const postSubject = async (newSubject) => {
    const { token } = isAuthenticated();
    const res = await axios.post(`${REACT_APP_APILOCAL}/add-subject`, {
        "name": newSubject.name
    },
        {
            headers: {
                Authorization: token
            }
        }
    );
    return res
}

export const removeSubject = async (id) => {
    const { token } = isAuthenticated();
    const res = await axios.delete(`${REACT_APP_APILOCAL}/remove-subject/${id}`, {
        headers: {
            Authorization: token
        }
    });
    return res;
}

export const updateSubject = async (id, newSubject) => {
    const { token } = isAuthenticated();
    const res = await axios.put(`${REACT_APP_APILOCAL}/update-subject/${id}`, {
        "name": newSubject.name
    }, {
        headers: {
            Authorization: token
        }
    });
    return res;
}
