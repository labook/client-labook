export const stepNumber = (stepResearches) => {
    return stepResearches.length === 0 ? 1 : stepResearches[stepResearches.length - 1].step_number + 1;
}