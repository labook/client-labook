import Swal from 'sweetalert2';

export const SweetModalResearch = async (title) => {
    const { value: formValues } = await Swal.mixin({
        input: 'text',
        confirmButtonText: 'Siguiente &rarr;',
        showCancelButton: true,
        progressSteps: ['1', '2', '3']
    }).queue([
        {
            title,
            text: 'Nombre que desee asignar a la investacion'
        },
        'Descripcion',
        {
            inputPlaceholder: '2020-07-14T00:00:00.000Z',
            text: 'Fecha de entrega'
        }
    ])


    return {
        name: formValues[0],
        description: formValues[1],
        deliver_date: formValues[2]
    };
}