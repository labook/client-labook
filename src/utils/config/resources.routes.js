import { AddEquipment } from "../../components/resources/resourcesTypes/equipments/AddEquipment";
import { ResourceEquipment } from "../../components/resources/resourcesTypes/equipments/ResourceEquipment";
import { UpdateEquipment } from "../../components/resources/resourcesTypes/equipments/UpdateEquipment";
import { AddMaterial } from "../../components/resources/resourcesTypes/material/AddMaterial";
import { ResourceMaterial } from "../../components/resources/resourcesTypes/material/ResourceMaterial";
import { UpdateMaterial } from "../../components/resources/resourcesTypes/material/UpdateMaterial";
import { AddReagent } from "../../components/resources/resourcesTypes/reagent/AddReagent";
import { ResourceReagent } from "../../components/resources/resourcesTypes/reagent/ResourceReagent";
import { UpdateReagent } from "../../components/resources/resourcesTypes/reagent/UpdateReagent";

export const ResourcesRoutes = [
    {
        path: "/ui/resources/materials",
        component: ResourceMaterial,
        exact: true,
    },
    {
        path: "/ui/resources/AddMaterial",
        component: AddMaterial,
        exact: true,
    },
    {
        path: "/ui/resources/UpdateMaterial/:id",
        component: UpdateMaterial,
        exact: true,
    },
    {
        path: "/ui/resources/AddReagent",
        component: AddReagent,
        exact: true,
    },
    {
        path: "/ui/resources/reagents",
        component: ResourceReagent,
        exact: true,
    },
    {
        path: "/ui/resources/UpdateReagent/:id",
        component: UpdateReagent,
        exact: true,
    },
    {
        path: "/ui/resources/equipments",
        component: ResourceEquipment,
        exact: true,
    },
    {
        path: "/ui/resources/AddEquipment",
        component: AddEquipment,
        exact: true,
    },
    {
        path: "/ui/resources/UpdateEquipment/:id",
        component: UpdateEquipment,
        exact: true,
    }
]