import { AddEquipment } from "../../components/resources/resourcesTypes/equipments/AddEquipment";
import { ResourceEquipment } from "../../components/resources/resourcesTypes/equipments/ResourceEquipment";
import { UpdateEquipment } from "../../components/resources/resourcesTypes/equipments/UpdateEquipment";
import { AddMaterial } from "../../components/resources/resourcesTypes/material/AddMaterial";
import { ResourceMaterial } from "../../components/resources/resourcesTypes/material/ResourceMaterial";
import { UpdateMaterial } from "../../components/resources/resourcesTypes/material/UpdateMaterial";
import { AddReagent } from "../../components/resources/resourcesTypes/reagent/AddReagent";
import { ResourceReagent } from "../../components/resources/resourcesTypes/reagent/ResourceReagent";
import { UpdateReagent } from "../../components/resources/resourcesTypes/reagent/UpdateReagent";
import { ResourceSelection } from "../../components/resources/resourcesTypes/selection/ResourceSelection";

export const ExperimentalDesignRoutes = [
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/materials",
        component: ResourceMaterial,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/AddMaterial",
        component: AddMaterial,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/UpdateMaterial/:id",
        component: UpdateMaterial,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/AddReagent",
        component: AddReagent,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/reagents",
        component: ResourceReagent,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/UpdateReagent/:id",
        component: UpdateReagent,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/equipments",
        component: ResourceEquipment,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/AddEquipment",
        component: AddEquipment,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/UpdateEquipment/:id",
        component: UpdateEquipment,
        exact: true,
    },
    {
        path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroupResearch/resources/resource-selection",
        component: ResourceSelection,
        exact: true,
    }
]