import React from 'react';
import Home from '../../components/Home/Home';
import ExperimentalDesign from '../../components/ExperimentalDesign/ExperimentalDesign';
import { SubjectScreen } from '../../components/subject/SubjectScreen';
import { CommissionsScreen } from '../../components/commissions/CommissionsScreen';
import { UpdateCommssionForm } from '../../components/commissions/UpdateCommssionForm';
import { AddCommissionForm } from '../../components/commissions/AddCommissionForm';
import { GroupsScreen } from '../../components/groups/GroupsScreen';
import { GroupCreateForm } from '../../components/groups/GroupCreateForm';
import { ResourceScreen } from '../../components/resources/ResourceScreen';
import { UpdateGroup } from '../../components/groups/UpdateGroup';
import { SignIn } from '../../components/Login/SignIn';
import { ResearchScreen } from '../../components/Research/ResearchScreen';
import { Redirect } from 'react-router-dom';
import { ExperimentalDesignRoutes } from './experimentalDesign.routes';
import { ResourcesRoutes } from './resources.routes';
import { TeacherCommission } from '../../components/teacherCommission/TeacherCommission';
import { ResearchStudent } from '../../components/Research/student/ResearchStudent';
import { StatementPDF } from '../../components/Research/StatementPDF';

const routes = [
    {
        path: "/ui",
        component: Home,
        exact: false,
        routes: [
            {
                path: "/ui",
                exact: true,
                component: () => <Redirect to="/ui/researchs" />
            },
            {
                path: "/ui/researchs",
                component: ResearchScreen,
                exact: true
            },
            {
                path: "/ui/research/:idResearch/pdf",
                component: StatementPDF,
                exact: true
            },
            {
                path: "/ui/student/researchs/:idStudent",
                component: ResearchStudent,
                exact: true
            },
            {
                path: "/ui/experimentalDesign/researchID/:researchID/groupId/:idGroup",
                component: ExperimentalDesign,
                exact: false,
                routes: ExperimentalDesignRoutes
            },

            {
                path: "/ui/subjects",
                component: SubjectScreen,
                exact: true
            },
            {
                path: "/ui/commissions",
                component: CommissionsScreen,
                exact: true
            },
            {
                path: "/ui/commissionGroups/:id",
                component: GroupsScreen,
                exact: true
            },
            {
                path: "/ui/commissionGroups/edit/:idGroup/:idCommision",
                component: UpdateGroup,
                exact: true
            },
            {
                path: "/ui/commissions/form-update/:id",
                component: UpdateCommssionForm,
                exact: true
            },
            {
                path: "/ui/commissions/form-newcomission",
                component: AddCommissionForm,
                exact: true
            },
            {
                path: "/ui/commissionsGroups/:id/creategroup",
                component: GroupCreateForm,
                exact: true
            },
            {
                path: "/ui/teacher/commissions",
                component: TeacherCommission,
                exact: true
            },
            {
                path: "/ui/resources",
                component: ResourceScreen,
                exact: false,
                routes: ResourcesRoutes
            },
            // {
            //     component: () => <Redirect to="/ui" />
            // }
        ],
    },
    {
        path: "/login",
        component: SignIn,
        exact: true
    },
    {
        component: () => <Redirect to="/login" />
    }
]

export default routes;