import M from "materialize-css/dist/js/materialize.min.js";
import "./materialize.css";

class MaterializeJs {

  modalInit() {
    var elems = document.querySelectorAll('.modal');
    M.Modal.init(elems);
  }
  selectOption() {
    var elems = document.querySelectorAll('select');
    M.FormSelect.init(elems);
  }

  tooltip = () => {
    const elems = document.querySelectorAll(".tooltipped");
    M.Tooltip.init(elems);
  }

  tooltipClose = () => {
    const elems = document.querySelectorAll(".tooltipped");
    var instance = M.Tooltip.getInstance(elems);
    instance.close();
  }

  dropDownInit = () => {
    var elems = document.querySelectorAll('.dropdown-trigger');
    M.Dropdown.init(elems, {
      coverTrigger: false,
      constrainWidth: false
    });
  }

  sideNav = () => {
    const sidenav = document.querySelector(".sidenav");
    M.Sidenav.init(sidenav);
  }

  getCarousel = () => {
    var elems = document.querySelectorAll(".carousel");
    M.Carousel.init(elems, {
      fullWidth: true,
      indicators: true,
    });
  }

  collapsible = () => {
    var elem = document.querySelectorAll('.collapsible');
    M.Collapsible.init(elem, {
      accordion: true
    });
  }

  toast = (message, className) => {
    M.toast({ html: message, classes: className, outDuration: 200, displayLength: 2000 });
  }

  inputUpdateTextFields = () => {
    M.updateTextFields();
  }
}

let materializeJs = new MaterializeJs();
export { materializeJs };
