import React from "react";

export const FormAuth = ({ inputValues, handleInputChange, onSignIn }) => {
  return (
    <>
      <form onSubmit={(e) => onSignIn(e, inputValues)}>
        <div className="row">
          <div className="input-field col s12">
            <i className="material-icons prefix">account_circle</i>
            <input
              id="dni_input"
              name="dni"
              type="number"
              className="validate"
              value={inputValues.dni}
              onChange={handleInputChange}
            />
            <label htmlFor="dni_input">Dni</label>
          </div>

          <div className="input-field col s12">
            <i className="material-icons prefix">lock</i>
            <input
              id="password_input"
              name="password"
              type="password"
              className="validate"
              value={inputValues.password}
              onChange={handleInputChange}
            />
            <label htmlFor="password_input">Password</label>
          </div>
          <div className="col s12">
            <label>
              <input type="checkbox" className="filled-in" checked="checked" />
              <span>Recordar contraseña</span>
            </label>
          </div>
        </div>
        <div className="row">
          <div className="col s12">
            <b className="cursor-pointer">Olvidaste tu password?</b>
          </div>
        </div>
        <div className="row">
          <div className="col s6">
            <a>Registrarte</a>
          </div>
          <div className="col s6">
            {/* <Link to={"/home/resources"} className="waver-effect waves-light btn" onClick={this.login}>Iniciar Sesión</Link> */}
            <button
              type="submit"
              className="waver-effect waves-light btn"
              // className={isLoading ? "waver-effect waves-light btn" : "btn disabled cursor-pointer"}
            >
              Iniciar Sesión
            </button>
          </div>
        </div>
      </form>
    </>
  );
};
