import React from 'react';
import "./Signin.css";
import logo_unahur from "../../assets/img/png/logo_unahur.png";
import { useHistory } from 'react-router-dom';
import { useForm } from '../../hooks/useForm';
import { FormAuth } from './FormAuth';
import { signinService, authenticateService } from '../../utils/services/auth/authService';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';

export const SignIn = () => {

    const [inputValues, handleInputChange] = useForm({
        dni: '',
        password: ''
    })
    const history = useHistory();

    const toastrMessage = (msg) => {
        toast.error(msg, {
            draggablePercent: 60,
        });
    }

    const isValidForm = () => {
        const { dni, password } = inputValues;

        if (dni.length === 0 || password.length === 0) {
            return false;
        }
        return true;
    }

    const onSignIn = async (e, user) => {
        e.preventDefault();
        try {
            if(isValidForm()){
                const { data } = await signinService({ ...user });
                authenticateService(data);
                history.push('/ui');
            }
            else{
                toastrMessage("Todos los campos soon requeridos")
            }

        }
        catch ({ response }) {
            console.log(response)
            toastrMessage(response.data.error)
        }
    }

    return (
        <>
            <div className="container">
                <div className="login-div">

                    <div className="row">
                        <div className="logo">
                            <img src={logo_unahur} className="responsive-img" />
                        </div>
                    </div>
                    <div className="row center-align mt10">
                        <h6>Iniciar Sesión</h6>
                    </div>

                    <FormAuth
                        inputValues={inputValues}
                        handleInputChange={handleInputChange}
                        onSignIn={onSignIn}
                    />
                </div>
                <ToastContainer draggablePercent={60} />
            </div>
        </>
    )
}