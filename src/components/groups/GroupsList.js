import React from 'react'
import { GroupsItem } from './GroupsItem'

export const GroupsList = ({ getGroupState, idCommission }) => {
    return (
        <div className="row">
            <div className="col s12 m10 offset-m1">
                {getGroupState.map((group, i) =>
                    <div className="col s12 m4" key={group.id}>
                        <GroupsItem {...group} idCommission={idCommission} />
                    </div>
                )}
            </div>
        </div>
    )
}
