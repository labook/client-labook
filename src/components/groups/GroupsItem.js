import React from 'react';
import { Link, useHistory } from 'react-router-dom';

export const GroupsItem = ({ id, name, student_groups, idCommission, group_researches }) => {

    const history = useHistory();

    const navigate = () => {
        history.push(`/ui/experimentalDesign/researchID/${group_researches[0].id_research_fk}/groupId/${group_researches[0].id_group_fk}`)
    }
    return (
        <div className="card blue-grey darken-1">
            <div className="card-content white-text height-172">
                <div className="card-title">
                    <span>{name}</span>
                    <Link to={`/ui/commissionGroups/edit/${id}/${idCommission}`}>
                        <i className="material-icons right white-text">edit</i>
                    </Link>
                </div>
                <strong className="mb50">Integrantes: </strong>
                {
                    student_groups.map(student =>
                        <p className="pt">{student.student.name} {student.student.lastname}</p>
                    )
                }

            </div>
            <div className="card-action">
                {/* <Link
                    to={`/ui/experimentalDesign/researchID/${group_researches[0].id_research_fk}/groupId/${group_researches[0].id_group_fk}`}
                >
                    Ver investigaciones
                </Link> */}
                <a onClick={() => navigate()}>Ver investigaciones</a>
            </div>
        </div>
    )
}
