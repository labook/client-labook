import React, { useEffect, useState } from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useForm } from '../../hooks/useForm';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';
import { getGroupService, updateGroupService, deleteGroupService } from '../../utils/services/GroupsService';
import { getResearchsService } from '../../utils/services/ResearchsService';
import { deleteStudentGroupService, postStudentGroup } from '../../utils/services/studentGroupService';
import { getStudentService } from '../../utils/services/StudentService';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
import swal from 'sweetalert';

export const UpdateGroup = () => {
    const [loading, setLoading] = useState(false);
    const [groupState, setgroupState] = useState({});
    const [researchState, setresearchState] = useState([]);
    const [studentState, setstudentState] = useState([]);
    const [studentSelectedState, setstudentSelectedState] = useState([]);
    const history = useHistory();
    const { idGroup, idCommision } = useParams();
    const [inputValues, handleInputChange, resetForm, handleCheckedChange, handleInputChangeFile, setInputsValues] = useForm({
        name: '',
        id_research_fk: null,
        id_comission_fk: null,
    });

    useEffect(() => {
        getGroup(idGroup);
        getResearchs();
        getStudents(idCommision);
    }, []);

    useEffect(() => {
        materializeJs.selectOption();
    });

    const getGroup = async (id) => {
        setLoading(true);
        const { data } = await getGroupService(id);
        setInputsValues({
            name: data.name,
            id_research_fk: data.id_research_fk
        })
        console.log(data)
        setgroupState(data);
        setLoading(false);
    }

    const getResearchs = async () => {
        setLoading(true);
        const { data } = await getResearchsService();
        setresearchState(data);
        setLoading(false);
    }

    const getStudents = async (id) => {
        setLoading(true);
        const { data } = await getStudentService(id);
        const studentsOnGroup = data.filter(student => !student.status)
        setstudentState(studentsOnGroup);
        setLoading(false)
    }

    const handleChangeStudents = ({ target }) => {
        const studentSelecteds = Array.from(
            target.selectedOptions,
            (option) => option.value //Mapea las selecciones elegidas 
        )
        setstudentSelectedState(studentSelecteds);
    }

    const updateStudentGroup = async (id, { name }) => {
        try {
            setLoading(true);
            const research = (inputValues.id_research_fk === null)
                ? groupState.research.id
                : inputValues.id_research_fk;

            await updateGroupService(id, {
                name,
                id_research_fk: research,
                id_comission_fk: Number(idCommision)
            });


            Promise.all(studentSelectedState.map(async student =>
                await postStudentGroup(Number(student), idGroup)
            ));


            setLoading(false);
            swal({
                text: 'Se ha actualizado el correctamente',
                icon: "success",
            }).then(willacept => {
                if (willacept) {
                    history.push(`/ui/commissionGroups/${idCommision}`);
                    resetForm();
                };
            });
        }
        catch (e) {
            setLoading(false);
            swal({
                title: "Error",
                text: "Algo ha ocurrido",
                icon: "error",
            });
        }
    }
    // Esta funcion borra la relacion del estudiante con el grupo
    // El id que recibe es el de la relacion entre el estudiante y el grupo
    const deleteStudentGroup = async (id) => {
        await deleteStudentGroupService(id);
        getStudents(idCommision);
        getGroup(idGroup);
    }
    // esta funcion borra el grupo entero y recibe el id del grupo
    // para borrar el grupo primero se debe borrar las relaciones del studentgroup
    const deleteGroup = async (id) => {
        swal(
            `¿Eliminar grupo ${groupState.name}?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        ).then(async willDelete => {
            if (willDelete) {
                await Promise.all(groupState.student_groups.map(async studenGroup =>
                    await deleteStudentGroupService(studenGroup.id)
                ))
                await deleteGroupService(id);

                history.push(`/ui/commissionGroups/${idCommision}`);
            };
        });
    }

    return (
        <>
            {
                loading &&
                <SpinnerOrbits />
            }

            <div className="row">
                <div className="col m5 s12 mt30">
                    <Link
                        to={`/ui/commissionGroups/${idCommision}`}
                        className="breadcrumb cyan-text darken-4"
                    >
                        Grupos de la comision
                    </Link>
                    <Link className="breadcrumb cyan-text darken-4">Editar grupo</Link>
                </div>
            </div>
            <center><h1>Editar {groupState.name}</h1></center>
            <div className="row mt60">
                <form className="col s12 m8 offset-m2">

                    <div className="input-field">
                        <input
                            autoFocus
                            autoComplete="off"
                            id="name"
                            type="text"
                            name="name"
                            onChange={handleInputChange}
                            value={inputValues.name}
                        />
                        <label htmlFor="name">Nombre del grupo</label>
                    </div>

                    <div class="input-field">
                        <select
                            name="id_research_fk"
                            value={inputValues.id_research_fk}
                            onChange={handleInputChange}
                        >
                            {
                                <option
                                    selected
                                >
                                    selecionar investigacion
                                </option>
                            }
                            {
                                // researchState.filter(research => research.id !== groupState.research.id)
                                researchState.map(research =>
                                    <option
                                        key={research.id}
                                        value={research.id}
                                    >
                                        {research.name}
                                    </option>
                                )
                            }
                        </select>
                        <label>Seleccionar investigacion</label>
                    </div>

                    <div class="input-field mt30">
                        <select
                            value={studentSelectedState}
                            onChange={handleChangeStudents}
                            multiple
                            disabled={studentState.length === 0 ? true : false}
                        >
                            {
                                (studentSelectedState.length === 0) &&
                                (studentState.length > 0) &&
                                <option value="" disabled selected>Integrantes disponibles</option>
                            }
                            {
                                studentState.length === 0 &&
                                <option value="" disabled selected>No hay alumnos disponibles</option>
                            }

                            {
                                studentState.map(student =>
                                    <option
                                        key={student.id}
                                        value={student.id}
                                    >
                                        {student.name} - {student.lastname}
                                    </option>
                                )
                            }
                        </select>
                        <label>Estudiantes disponibles</label>
                    </div>

                    <span className="pt70">Integrantes</span>
                    <div class="collection">
                        {
                            groupState.student_groups &&
                                groupState.student_groups.length > 0 ?
                                groupState.student_groups.map(studenGroup =>
                                    <a href="#!" className="collection-item">
                                        <i
                                            className="material-icons red-text right"
                                            onClick={() => deleteStudentGroup(studenGroup.id)}
                                        >
                                            delete
                                    </i>
                                        {studenGroup.student.name}
                                    </a>
                                )
                                : <p className="ml20">Este grupo aún no tiene Integrantes</p>
                        }
                    </div>

                    <div className="col s12">
                        <a
                            className="btn cyan darken-3 waves-effect waves-light btn-small right my30"
                            onClick={() => updateStudentGroup(idGroup, inputValues)}
                        >
                            Enviar
                        </a>
                    </div>
                </form>
            </div>
            <a
                className="fixed-action-btn"
                onClick={() => deleteGroup(idGroup)}
            >
                <div className="btn-floating btn-large waves-effect waves-light red">
                    <i className="material-icons">delete</i>
                </div>
            </a>
        </>
    )
}
