import React, { useEffect, useState } from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
// Custom Hook
import { useForm } from '../../hooks/useForm';
// Components
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
// Services
import { getCommissionService } from '../../utils/services/CommissionService';
import { getResearchsService } from '../../utils/services/ResearchsService';
import { getStudentService } from '../../utils/services/StudentService';
import { postGroupsService, postGroupResearchService } from '../../utils/services/GroupsService';
import swal from 'sweetalert';
import { postStudentGroup } from '../../utils/services/studentGroupService';

export const GroupCreateForm = (props) => {

    const [loading, setLoading] = useState(false);
    const [getCommissionState, setCommissionState] = useState({});
    const [getStudentState, setStudentState] = useState([]);
    const [researchState, setresearchState] = useState([]);
    const { id: idCommission } = useParams();
    const history = useHistory();
    const [studentSelectedState, setstudentSelectedState] = useState([]);
    const [inputValues, handleInputChange, resetForm] = useForm({
        name: '',
        id_research_fk: null,
        id_comission_fk: null,
    });

    useEffect(() => {
        getCommission(idCommission);
        getStudentComission(idCommission);
        getResearchs();
        console.log(idCommission)
    }, []);

    useEffect(() => {
        materializeJs.selectOption();
    });

    const getCommission = async (id) => {
        setLoading(true);
        const { data } = await getCommissionService(id);
        setCommissionState(data);
        setLoading(false);
    }

    const getStudentComission = async (id) => {
        setLoading(true);
        const { data } = await getStudentService(id);
        console.log(data)
        setStudentState(data);
        setLoading(false);
    }

    const getResearchs = async () => {
        setLoading(true);
        const { data } = await getResearchsService();
        setresearchState(data);
        setLoading(false);
    }

    const addGroupCommission = async (newGroup) => {
        try {
            const { data } = await postGroupsService({
                ...newGroup,
                id_comission_fk: idCommission
            });

            await postGroupResearchService(data.id, inputValues.id_research_fk)

            studentSelectedState.map(async student =>
                await postStudentGroup(Number(student), data.id)
            );

            swal({
                title: newGroup.name,
                text: "Creado correctamente",
                icon: "success",
            }).then(willacept => {
                if (willacept) {
                    history.push(`/ui/commissionGroups/${idCommission}`);
                    resetForm();
                };
            });
        }
        catch (e) {
            swal({
                title: "Error",
                text: "Algo ha ocurrido",
                icon: "error",
            });
        }
    }

    const handleChangeStudents = ({ target }) => {
        const studentSelecteds = Array.from(
            target.selectedOptions,
            (option) => option.value //Mapea las selecciones elegidas 
        )
        setstudentSelectedState(studentSelecteds)
    }

    return (
        <>
            {
                loading &&
                <SpinnerOrbits />
            }
            {
                !loading &&
                <div>
                    <div className="row">
                        <div className="col m5 s12 mt30">
                            <Link
                                to={"/ui/commissions"}
                                className="breadcrumb cyan-text darken-4"
                            >
                                Comisiones
                            </Link>
                            <Link
                                to={`/ui/commissionGroups/${idCommission}`}
                                className="breadcrumb cyan-text darken-4"
                            >
                                {getCommissionState.description}
                            </Link>

                            <Link className="breadcrumb cyan-text darken-4">Creacion de grupo</Link>
                        </div>
                        <div className="col m4 s12 offset-s2">
                            <h5 className="mt30">Crear un nuevo grupo</h5>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col m6 s12 offset-m3 mt30 mx20">
                            <div class="input-field">
                                <input
                                    autoFocus
                                    autoComplete="off"
                                    className="validate"
                                    name="name"
                                    onChange={handleInputChange}
                                    placeholder="Nombre del grupo"
                                    type="text"
                                    value={inputValues.name}
                                />
                            </div>
                        </div>
                        <div class="input-field col m6 s12 offset-m3">
                            <select
                                value={studentSelectedState}
                                onChange={handleChangeStudents}
                                multiple
                            >
                                {
                                    studentSelectedState.length === 0 &&
                                    <option value="" disabled selected>Integrantes disponibles</option>
                                }
                                {
                                    getStudentState.map(student =>
                                        <option
                                            key={student.id}
                                            value={student.id}
                                        >
                                            {student.name} - {student.lastname}
                                        </option>
                                    )
                                }
                            </select>
                            <label>Seleccion de integrantes</label>
                        </div>

                        <div class="input-field col m6 s12 offset-m3">
                            <select
                                name="id_research_fk"
                                value={inputValues.id_research_fk}
                                onChange={handleInputChange}
                            >
                                <option value="" disabled selected>Elegir investgacion para el grupo</option>
                                {
                                    researchState.map(research =>
                                        <option
                                            key={research.id}
                                            value={research.id}
                                        >
                                            {research.name}
                                        </option>
                                    )
                                }
                            </select>
                            <label>Seleccionar investigacion</label>
                        </div>

                        <div className="col m9 s12">
                            <a
                                className="btn-flat white-text cyan darken-3 waves-effect waves-light btn-small right my30"
                                onClick={() => addGroupCommission(inputValues)}
                            >
                                Enviar
                            </a>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}
