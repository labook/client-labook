import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';
import { getCommissionService } from '../../utils/services/CommissionService';
import { getGroupsService } from '../../utils/services/GroupsService';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
import { GroupsList } from './GroupsList';

export const GroupsScreen = () => {

    const [getGroupState, setGetGroupState] = useState([]);
    const [getCommissionState, setGetCommissionState] = useState({});
    const [loading, setLoading] = useState(false);
    const { id: idCommission } = useParams();

    useEffect(() => {
        getCommission(idCommission);
        getGroups(idCommission);
        materializeJs.tooltip();
    }, []);

    useEffect(() => {
        materializeJs.tooltip();
    });

    const { description, days, subject } = getCommissionState;

    const getGroups = async (idCommission) => {
        setLoading(true);
        const { data } = await getGroupsService(idCommission);
        setGetGroupState(data);
        setLoading(false);
    }

    const getCommission = async (id) => {
        setLoading(true);
        const { data } = await getCommissionService(id);
        setGetCommissionState(data);
        setLoading(false);
    }

    return (
        <>
            {
                loading && <SpinnerOrbits />
            }
            {
                !loading &&
                <div>
                    <div className="row">
                        <div className="col m10 s12 offset-m1">
                            <Link to={"/ui/commissions"} className="breadcrumb cyan-text darken-4">Comisiones</Link>
                            <Link className="breadcrumb cyan-text darken-4">{description}</Link>
                            <ul className="collection">
                                <li className="collection-item">
                                    <h5>{description}</h5>
                                    {
                                        subject &&
                                        <strong>{subject.name}</strong>
                                    }
                                    <br />
                                    <span>{days}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {
                        getGroupState.length > 0 ?
                            <GroupsList
                                getGroupState={getGroupState}
                                idCommission={idCommission}
                            />
                            :
                            <center>
                                <p>Aun no se crearon grupos</p>
                            </center>
                    }

                    <div
                        className="fixed-action-btn tooltipped"
                        data-tooltip="Crear un nuevo grupo"
                        data-position="left"
                    >
                        <Link
                            to={`/ui/commissionsGroups/${idCommission}/creategroup`}
                            className="btn-floating btn-large cyan darken-4"
                        >
                            <i className="large material-icons">add</i>
                        </Link>
                    </div>
                </div>
            }
        </>
    )
}
