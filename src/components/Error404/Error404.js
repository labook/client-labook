import React, { useEffect } from 'react';
// React Router
import { NavLink, useHistory } from 'react-router-dom';
// Redux
import { connect } from "react-redux";
function Error404({darkMode}) {

    const history = useHistory();

    useEffect(() => {
        // history.push('/ui/resources/materials');
    }, []);

    return (
        <center className="mt300">
            <code className={darkMode ? "white-text mb20":"mb20"}>PAGE NOT FOUND</code>
            <br />
            {/* <NavLink to={"/ui/resources"}>Ir al Home</NavLink> */}
        </center>
    )
}

const mapStateToProps = (state) => {
    return {
        darkMode: state.darkMode,
        user: state.user
    }
}


export default connect(mapStateToProps)(Error404)
