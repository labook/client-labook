import React, { useEffect } from 'react';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import { getResearchsService } from '../../../utils/services/ResearchsService';
import { getStudentByIdService } from '../../../utils/services/StudentService';
import { ResearchItem } from '../ResearchItem';


export const ResearchStudent = () => {

    const { idStudent } = useParams();
    const [studentResearchs, setstudentResearchs] = useState([]);
    const [student, setStudent] = useState({});

    useEffect(() => {
        studentResearch();
        dataStudent();
    }, [])

    const studentResearch = async () => {
        const { data } = await getResearchsService(idStudent);
        setstudentResearchs(data);
    }

    const dataStudent = async () => {
        const { data } = await getStudentByIdService(idStudent);
        setStudent(data);
    }

    return (
        <>
            <div className="row">
                {
                    studentResearchs.length > 0 ?
                        studentResearchs.map(research =>
                            <ResearchItem research={research} />
                        )
                        :
                        <center className="pt50">
                            <h6>El alumno <span className="teal-text cursor-pointer">{student.name}</span> no tiene investigaciones asignadas.</h6>
                        </center>
                }
            </div>
        </>
    )
}
