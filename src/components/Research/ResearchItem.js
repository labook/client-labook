import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useFileUpload } from 'use-file-upload';
import { Link, useParams } from 'react-router-dom';
import { isAuthenticated } from '../../utils/services/auth/authService';
import { addStatementPdfService, getStatementService } from '../../utils/services/statementService';

export const ResearchItem = ({ research, deleteResearch, updateResearch }) => {

    const { rol } = isAuthenticated();
    const [file, selectFile] = useFileUpload();
    const [pdf, setPdf] = useState([]);

    useEffect(() => {
        statementService();
    }, [])

    useEffect(() => {
        const addStatementPdf = async () => {
            await addStatementPdfService(file, research.id)
        }
        file && addStatementPdf();
    }, [file])

    const statementService = async () => {
        let id = research.id ? research.id : research.research_id;
        const { data } = await getStatementService(id);
        setPdf(data);
    }

    return (
        <>
            <div className="col s12 m4">
                <div className="card">
                    <div className="card-content">
                        <span className="card-title activator grey-text darken-4">
                            {
                                rol.rol_name === 'student' &&
                                <Link
                                    to={`/ui/experimentalDesign/researchID/${research.research_id}/groupId/${research.group_id}`}
                                    className="black-text"
                                >
                                    {research.research_name}
                                </Link>
                            }

                            {
                                rol.rol_name === 'teacher' &&
                                <Link
                                    // to={`/ui/experimentalDesign/researchID/${research.research_id}/groupId/${research.group_id}`}
                                    className="black-text"
                                >
                                    {research.name}
                                </Link>
                            }

                            {
                                pdf.length > 0 ?
                                    <Link
                                        to={`/ui/research/${research.research_id || research.id}/pdf`}
                                        className="waves-effect waves-light btn btn-small right blue"
                                    >
                                        <i className="material-icons right">picture_as_pdf</i> Ver
                                    </Link>
                                    :
                                    // <a
                                    //     className="waves-effect waves-light btn btn-small right blue"
                                    //     onClick={()=> }
                                    // >
                                    //     <i className="material-icons right">picture_as_pdf</i> Subir
                                    // </a>

                                    rol.rol_name === 'teacher' &&
                                    <span
                                        className="waves-effect waves-light btn btn-small right blue"
                                        onClick={() => selectFile()}
                                    >
                                        <i className="material-icons right">picture_as_pdf</i> Subir
                                    </span>


                            }

                        </span>
                        <p className="grey-text lighten-2 fsize13 pt20">{research.description}</p>
                        <p className="grey-text lighten-2 fsize13 pt20">Fecha de entrega: {research.deliver_date}</p>
                    </div>

                    {
                        rol.rol_name === 'teacher' &&
                        <div class="card-action">
                            <a href="#" onClick={() => updateResearch(research.id)}>Actualizar</a>

                            <a href="#" className="right red-text" onClick={() => deleteResearch(research.id)}>
                                <i className="material-icons">
                                    delete
                                </i>
                            </a>

                            {/* <a href="#">Ver Investigacion</a> */}
                        </div>
                    }

                    {
                        <div className="card-action">
                            <strong className="pt-20">{research.group_name}</strong>
                        </div>
                    }
                </div>
            </div>
        </>
    )
}
