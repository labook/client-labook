import React, { useEffect, useState } from "react";
import {
  createResearchService,
  deleteResearchService,
  getResearchsService,
  updateResearchService,
} from "../../utils/services/ResearchsService";
import { SweetModalResearch } from "../../utils/helpers/researchHelper";
import Swal from "sweetalert2";
import { isAuthenticated } from "../../utils/services/auth/authService";
import { ResearchItem } from "./ResearchItem";
import { removeStatementService } from "../../utils/services/statementService";

export const ResearchScreen = () => {
  const { rol } = isAuthenticated();
  const [researchsState, setResearchsState] = useState([]);

  useEffect(() => {
    getResearchs();
  }, []);

  const getResearchs = async () => {
    const { data } = await getResearchsService();
    setResearchsState(data);
  };

  const addResearch = async () => {
    try {
      const newResearch = await SweetModalResearch("Agregar investigacion");
      if (newResearch.name.length === 0) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Nombre de investigacion es requerido",
        });
      } else {
        await createResearchService({ ...newResearch });
        getResearchs();
      }
    } catch (e) {
      console.log(e);
    }
  };

  const deleteResearch = async (id) => {
    const result = await Swal.fire({
      title: "¿Eliminar investigacion permanentemente?",
      showCancelButton: true,
      confirmButtonText: `Eliminar`,
    });
    if (result.isConfirmed) {
      await removeStatementService(id);
      await deleteResearchService(id);
      getResearchs();
    }
  };

  const updateResearch = async (id) => {
    try {
      const newResearch = await SweetModalResearch("Actualizar investigacion");
      console.log(newResearch);
      if (newResearch.name.length === 0) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Nombre de investigacion es requerido",
        });
      } else {
        // await updateResearchService(id, { ...newResearch })
        getResearchs();
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <div className="row">
        {researchsState.length === 0 && (
          <h5 className="center-align mt70">
            Aun no hay investigaciones creadas.
          </h5>
        )}

        {researchsState.map((research) => (
          <ResearchItem
            key={research.id}
            research={research}
            deleteResearch={deleteResearch}
            updateResearch={updateResearch}
          />
        ))}

        {rol.rol_name === "teacher" && (
          <div className="fixed-action-btn" onClick={() => addResearch()}>
            <span className="waves-effect waves-light btn cyan darken-4">
              <i className="material-icons right">add</i>Agregar investigacion
            </span>
          </div>
        )}
      </div>
    </>
  );
};
