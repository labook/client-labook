import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { addStatementPdfService, getStatementService } from '../../utils/services/statementService';

export const StatementPDF = () => {
    const [pdf, setPdf] = useState([])
    const { idResearch } = useParams();

    useEffect(() => {
        if (typeof window.orientation !== "undefined") {
            document.getElementById('enlaceDescargarPdf').click();
            window.close();
        }
        statementService();
    }, [])
    
    const statementService = async () => {
        const { data } = await getStatementService(idResearch);
        console.log(data)
        setPdf(data);
    }
    
    return (
        <div>
            {
                pdf.length > 0 &&
                <div style={{ position: 'absolute', width: '100%', height: '100%' }}>
                    <object
                        data={pdf[pdf.length - 1].statement_pdf}
                        type="application/pdf"
                        width="100%"
                        height="100%"
                    >
                        <br />
                        <a href={pdf.statement_pdf} id="enlaceDescargarPdf"
                            download="ReactJS.pdf"
                        >Tu dispositivo no puede visualizar los PDF, da click aquí para descargarlo</a>
                    </object>
                </div>
            }

        </div>
    )
}
