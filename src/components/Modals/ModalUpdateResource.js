import React, { useState, useEffect } from 'react';

export default function ModalUpdateResource({ item }) {
    const [resourceItem, setResourceItem] = useState(null);

    const onChangeInput = ({target}) => {
        setResourceItem({ ...resourceItem, [target.name]: target.value });
    }
    useEffect(() => {
        console.log(item)
    //     item && setResourceItem(item)
        // return () => {
        //     cleanup
        // }
    }, [resourceItem])

    console.log(item)
    return (
        <div id="modalUpdateResource" className="modal">
            <div className="modal-content">
                <h5>Actualizar el recurso {item.name}</h5>
            </div>
            <div className="modal-footer">
                <div className="mx20">
                    <div className="row">
                        <form className="col s12">
                            <div className="row">
                                <div className="input-field col s12">
                                    <input id="name" type="text" className="validate" name="name" onChange={onChangeInput} />
                                    <label htmlFor="name">Nombre</label>
                                </div>
                                <div className="input-field col s12">
                                    <input id="description" name="description" type="text" className="validate" onChange={onChangeInput} />
                                    <label htmlFor="description">Descripccion</label>
                                </div>
                                <div class="file-field input-field col s12">
                                    <div class="btn purple">
                                        <span>File</span>
                                        <input type="file" multiple />
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Upload one or more files" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <a href="#!" className="modal-close btn-flat red lighten-1 white-text left">Cancelar</a>
                    <a href="#!" className="modal-close btn-flat white-text cyan">Aceptar</a>
                </div>
            </div>
        </div>
    )
}
