import React from 'react';

export default function ModalDeleteResource({deleteResource,item}) {
    return (
        <div id="modalDeleteResource" className="modal">
            <div className="modal-content">
                <h5>Eliminar {item.name} ?</h5>
            </div>
            <div className="modal-footer">
                <div className="mx20">
                    <a href="#!" className="modal-close btn-flat red lighten-1 white-text left">Cancelar</a>
                    <a href="#!" className="modal-close btn-flat white-text cyan" onClick={() => deleteResource(item)}>Aceptar</a>
                </div>
            </div>
        </div>
    )
}
