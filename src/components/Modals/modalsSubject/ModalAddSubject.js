import React from 'react';
import swal from 'sweetalert';
import { useForm } from '../../../hooks/useForm';
import { postSubject } from '../../../utils/services/SubjectService';

export const ModalAddSubject = ({ getSubjects }) => {

    const [inputValues, handleInputChange, resetForm] = useForm({
        name: ''
    });

    const handleAddSubject = async (subject) => {
        try{
            await postSubject(subject);
            getSubjects();
            resetForm();
        }
        catch ({ response }) {
            if (response) {
                swal({
                    title: "Error",
                    text: response.data.message,
                    icon: "error",
                });
            }
        }
    }

    return (
        <>
            <div id="modalSubject" className="modal">
                <div className="modal-content d-flex">
                    <center>
                        <h5 className="mb50">Agregar una nueva Materia</h5>
                    </center>
                    <div class="input-field">
                        <input
                            autoFocus
                            autoComplete="off"
                            className="validate"
                            id="name"
                            name="name"
                            onChange={handleInputChange}
                            placeholder="Nombre.."
                            type="text"
                            value={inputValues.name}
                        />
                        <label htmlFor="name">Nombre de la materia</label>
                    </div>
                </div>
                <div className="modal-footer">
                    <div className="mx20">
                        <a
                            className="modal-close btn-flat white-text waves-effect waves-light  btn-small red lighten-1 left"
                        >
                            Cancelar
                        </a>
                        <a
                            className="modal-close btn-flat white-text cyan darken-3 waves-effect waves-light btn-small"
                            onClick={() => handleAddSubject(inputValues)}
                        >
                            Agregar Materia
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}
