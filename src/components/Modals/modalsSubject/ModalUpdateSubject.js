import React from 'react';
import { useForm } from '../../../hooks/useForm';
import { useEffect } from 'react';
import { materializeJs } from '../../../utils/MaterializeJS/Materialize.service';
export const ModalUpdateSubject = ({ subjectSelect, setSubject }) => {

    const [inputValues, handleInputChange] = useForm({
        name: subjectSelect.name
    });

    useEffect(() => {
        // console.log('En useEffect, valor de materia inicial');
        // console.log(subjectSelect.name);
        materializeJs.inputUpdateTextFields();
        return () => {
            // console.log('Desmontando ModalUpdateSubject');
            // console.log(inputValues);
        }
    }, [subjectSelect]);


    return (
        <>
            <div id="modalupSubject" className="modal">
                <div className="modal-content d-flex">
                    <center>
                        <h5 className="mb50">Actualizar Materia {subjectSelect.name}</h5>
                    </center>
                    <div class="input-field">
                        <input
                            autoComplete="off"
                            autoFocus
                            className="validate active"
                            id="name"
                            name="name"
                            onChange={handleInputChange}
                            placeholder="Nombre.."
                            type="text"
                            value={inputValues.name}
                        />
                        <label htmlFor="name">Nombre de la materia</label>
                    </div>
                </div>
                <div className="modal-footer">
                    <div className="mx20">
                        <a
                            className="modal-close btn-flat white-text waves-effect waves-light  btn-small red lighten-1 left"
                        // onClick={() => resetForm()}
                        >
                            Cancelar
                        </a>
                        <a
                            className="modal-close btn-flat white-text cyan darken-3 waves-effect waves-light btn-small"
                            onClick={() => setSubject(subjectSelect.id, inputValues)}
                        >
                            Enviar
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}
