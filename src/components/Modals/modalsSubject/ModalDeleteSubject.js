import React from 'react'

export const ModalDeleteSubject = ({ id, name, deleteSubject }) => {
    return (
        <>
            <div id="modalDeleteSubject" className="modal">
                <div className="modal-content d-flex">
                    <center>
                        <h5 className="mb50">¿Desea eliminar la materia '{name}' de forma permanente?</h5>
                    </center>
                </div>
                <div className="modal-footer">
                    <div className="mx20">
                        <a
                            className="modal-close btn-flat white-text waves-effect waves-light btn-small red lighten-1 left"
                        // onClick={() => resetForm()}
                        >
                            Cancelar
                        </a>
                        <a
                            className="modal-close btn-flat white-text cyan darken-3 waves-effect waves-light btn-small"
                            onClick={() => deleteSubject(id)}
                        >
                            Eliminar
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}
