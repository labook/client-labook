import React, { Fragment } from 'react';
import { SubjectItem } from './SubjectItem';

export const SubjectList = ({ subjects, deleteSubject, setSubjectSelected }) => {

    return (
        <Fragment>
            {subjects.map((subject,i) =>
                <SubjectItem
                    key={i}
                    {...subject}
                    deleteSubject={deleteSubject}
                    setSubjectSelected={setSubjectSelected}
                />
            )}
        </Fragment>
    )
}
