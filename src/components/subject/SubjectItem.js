import React, { useEffect } from 'react';
import swal from 'sweetalert';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';

export const SubjectItem = ({ name, id, comissions, setSubjectSelected, deleteSubject }) => {

    useEffect(() => {
        materializeJs.tooltip();
    }, [])


    const modalSweetAlertDeleteSubject = () => {

        if (comissions.length > 0) return;
        swal(
            `¿Desea eliminar la materia '${name}' de forma permanente?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        )
            .then(async willDelete => {

                if (willDelete) {
                    await deleteSubject(id);
                    swal({
                        text: `${name} se eliminó correctamente`,
                        icon: "success"
                    });
                }

            })
            .catch(err => {
                swal({
                    text: err,
                    icon: "error"
                });
            });
    }

    return (
        <div
            className={"subjects__item my20 cursor-pointer"}
            onClick={() => setSubjectSelected(id)}
        >
            <span>{name}</span>

            <div>
                <span
                    className={comissions.length > 0 ? "btn-flat tooltipped" : "btn-flat tooltipped red-text"}
                    // href='#modalDeleteSubject'
                    data-tooltip={comissions.length > 0 ? "Para eliminar la materia primero debe eliminar la comision relacionada a la misma" : "Eliminar materia"}
                    onClick={() => modalSweetAlertDeleteSubject()}
                >
                    <i className="material-icons">close</i>
                </span>
            </div>
        </div >
    )
}
