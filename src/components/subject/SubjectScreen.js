import React, { useEffect, useState, useMemo, useCallback } from 'react';
import './subject.css';
import { SubjectList } from './SubjectList';
import { ModalAddSubject } from '../Modals/modalsSubject/ModalAddSubject';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';
import { getSubjects, getSubject, removeSubject, updateSubject } from '../../utils/services/SubjectService';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
import { ModalUpdateSubject } from '../Modals/modalsSubject/ModalUpdateSubject';
import { Link } from 'react-router-dom';

export const SubjectScreen = () => {

    const [subjects, setSubjects] = useState([]);
    const [loader, setLoader] = useState(false);
    const [subjectSelect, setSubjectSelect] = useState(null);

    useEffect(() => {
        materializeJs.modalInit();
        getSubjectsById();
    }, []);

    useEffect(() => {
        materializeJs.modalInit();
    }, [subjectSelect]);


    const getSubjectsById = async () => {
        setLoader(true);
        const { data } = await getSubjects();
        setSubjects(data);
        setLoader(false);
        console.log(data)
    }

    const deleteSubject = async (id) => {
        setLoader(true);
        await removeSubject(id);
        await getSubjectsById();
        setSubjectSelect(null);
        setLoader(false);
    }

    const setSubjectSelected = async (id) => {
        setLoader(true);
        const { data } = await getSubject(id);
        setSubjectSelect(data);
        setLoader(false);
    }

    const setSubject = async (id, newSubject) => {
        setLoader(true);
        await updateSubject(id, newSubject);
        getSubjectsById();
        setSubjectSelect(null);
        setLoader(false);
    }

    return (
        <div>
            {loader && <SpinnerOrbits />}

            {
                !loader &&
                <div>
                    <div className="row" >
                        <div className="col m5 s12 mt30 ">
                            <div className="subjects__left z-depth-3">
                                <div className="container">
                                    <h6 className="mt30">Materias</h6>
                                    <hr />
                                    <SubjectList
                                        subjects={subjects}
                                        deleteSubject={deleteSubject}
                                        setSubjectSelected={setSubjectSelected}
                                    />
                                    <a
                                        className="waves-effect waves-light btn btn-small cyan darken-3 mt10  modal-trigger btn-addSubject"
                                        href='#modalSubject'
                                    >
                                        Crear nueva materia
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div className="col m7 s12 mt30">
                            <div className="subjects__right z-depth-3">
                                <div className="subjects__right_centered">
                                    {
                                        !subjectSelect &&
                                        <center>
                                            <span>Seleccione una materia para ver su informacion</span>
                                        </center>
                                    }
                                </div>
                                {
                                    subjectSelect &&
                                    <div>
                                        <div className="subject__select-title">
                                            <div className="ml20">
                                                <h4 className="grey-text darken-4">{subjectSelect.name}</h4>
                                            </div>
                                            <div className="mr30 mt20">
                                                <a
                                                    className="btn-floating waves-effect waves-light cyan darken-3 right modal-trigger"
                                                    href="#modalupSubject"
                                                >
                                                    <i className="material-icons">edit</i>
                                                </a>
                                            </div>
                                        </div>

                                        <div className="mt30">
                                            {
                                                subjectSelect.comissions.length >= 1 &&
                                                <h6 className="ml20"><strong>Comisiones:</strong></h6>
                                            }
                                            {
                                                subjectSelect.comissions.length > 0
                                                && subjectSelect.comissions.map(comission =>
                                                    <div className="ml20 black-text mt10" key={comission.id}>
                                                        <strong>{comission.description}</strong>
                                                        <p className="ml50">Dias: {comission.days}</p>
                                                        <p className="ml50">Horarios: {comission.start_time} a {comission.end_time}</p>
                                                    </div>
                                                )
                                            }
                                            {
                                                subjectSelect.comissions.length <= 0 &&
                                                <div className="subject__title_commisions">
                                                    <p className="mt50">No Hay comisiones asignadas a esta materia.</p>
                                                    <center className="cursor-pointer cyan-text darken-4">
                                                        <Link to={'/ui/commissions'}>Asignar una comision</Link>
                                                    </center>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            }

            {
                subjectSelect &&
                <ModalUpdateSubject
                    subjectSelect={subjectSelect}
                    setSubject={setSubject}
                />
            }
            <ModalAddSubject
                getSubjects={getSubjectsById}
            />

        </div>
    )
}
