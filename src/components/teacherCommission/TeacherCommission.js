import React, { useEffect, useState } from 'react';
import '../commissions/commissions.css';
import { getCommissionsByteacher } from '../../utils/services/CommissionService';
import { CommissionsList } from '../commissions/CommissionsList';
import { Link } from 'react-router-dom';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
import { isAuthenticated } from '../../utils/services/auth/authService';

export const TeacherCommission = () => {

    const [commissions, setCommissions] = useState([]);
    const [loading, setloading] = useState(false);
    const { id_teacher_fk: idTeacher } = isAuthenticated();
    useEffect(() => {
        getComissions();
        // materializeJs.selectOption();
    }, []);

    const getComissions = async () => {
        setloading(true);
        const { data } = await getCommissionsByteacher(idTeacher);
        console.log(data)
        // setCommissions(data);
        setloading(false);
    }

    return (
        <div>
            {
                !loading &&
                <div>
                    <div className="row mt30">
                        <div className="col m3 offset-m2">
                            <Link
                                className="btn-floating waves-effect waves-light cyan darken-4 mt15"
                                to={'/ui/commissions/form-newcomission'}
                            >
                                <i className="material-icons">add</i>
                            </Link>
                        </div>
                        <div className="col m7">
                            {
                                commissions.length === 0 ?
                                    <div>
                                        <h5>No hay comisiones para mostrar</h5>
                                        <span className="ml100">Crear una nueva comision</span>
                                    </div>
                                    :
                                    <h5>Comisiones totales</h5>
                            }
                        </div>
                    </div>
                    <div className="row mt50">
                        <div className="col m8 offset-m2">
                            {
                                commissions.length > 0 &&
                                <CommissionsList
                                    commissions={commissions}
                                    getComissions={getComissions}
                                    setloading={setloading}
                                />
                            }
                        </div>
                    </div>
                </div>
            }

            {
                loading && <SpinnerOrbits />
            }
        </div>
    )
}
