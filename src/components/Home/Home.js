import React, { Component, useEffect } from "react";
import './Home.css';
// Materialize
import { materializeJs } from "../../utils/MaterializeJS/Materialize.service";
// Components
import Navbar from "../UI/NavBar/Navbar";
import SideBar from "../UI/SideBar/SideBar";
//React router
import { Switch, useHistory } from "react-router-dom";
// Redux
import { connect } from "react-redux";
import { RouteWithSubRoutes } from "../../utils/config/RouteWithSubRoutes";

const Home = ({ routes, darkMode }) => {
    // const history = useHistory();

    useEffect(() => {
        materializeJs.sideNav();
        materializeJs.collapsible();
        // history.push('/ui/resources/materials');
    }, []);

    return (
        <div className={darkMode ? "container-layout-dark" : "container-layout"}>
            <Navbar />
            <SideBar />

            <Switch>
                {routes.map((route, i) => (
                    <RouteWithSubRoutes key={i} {...route} />
                ))}
            </Switch>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        darkMode: state.darkMode,
        user: state.user
    }
}

export default connect(mapStateToProps)(Home)




