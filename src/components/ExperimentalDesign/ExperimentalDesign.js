import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import swal from 'sweetalert';
import { stepNumber } from '../../utils/helpers/experimentalDeignHelper';
import { getGroupService } from '../../utils/services/GroupsService';
import { getResearchService, createStepResearchService, deleteStepResearchService, getGroupResearchService } from '../../utils/services/ResearchsService';
import { ResourceScreen } from '../resources/ResourceScreen';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
import { HeaderExperimental } from './HeaderExperimental';
import { StepResearchItem } from './StepResearchItem';

export default function ExperimentalDesign({ routes }) {

    let history = useHistory();
    const { researchID, idGroup } = useParams();
    const [loading, setLoading] = useState(false);
    const [researchState, setResearchState] = useState({});
    const [groupResearchState, setGroupResearchState] = useState({
        id: '',
        step_researches: []
    });

    useEffect(() => {
        if (history.location.pathname === `/ui/experimentalDesign/researchID/${researchID}/groupId/${idGroup}`) {
            getReseacrhId()
            getGroupResearch()
        }
    }, []);

    const getReseacrhId = async () => {
        try {
            setLoading(true);
            const { data } = await getResearchService(researchID);
            setResearchState(data);
            setLoading(false);
        }
        catch (e) {
            setLoading(false);
            console.log(e)
        }
    }


    const getGroupResearch = async () => {
        try {
            setLoading(true);
            const { data } = await getGroupResearchService(Number(idGroup));
            setGroupResearchState(data.groupResearch.find(data => data.id_research_fk === Number(researchID)));
        }
        catch (e) {
            console.log(e)
        }
        finally {
            setLoading(false);
        }
    }

    const addStepResearch = async () => {

        swal({
            text: `Documentar paso`,
            content: {
                element: "input",
                attributes: {
                    placeholder: "Describir paso",
                    type: "text",
                }
            },
            buttons: {
                cancel: 'Cancelar',
                confirm: 'Crear'
            }
        })
            .then(async step => {
                if (!step) throw null;
                setLoading(true);
                console.log(idGroup)
                const newStep = {
                    step_number: stepNumber(groupResearchState.step_researches),
                    step_description: step,
                    teacher_check: true,
                    id_group_research_fk: groupResearchState.id
                }

                const { data } = await createStepResearchService({ ...newStep });
                getGroupResearch();
                setLoading(false);
                return data;
            })
            .catch(err => {
                swal({
                    text: err,
                    icon: "error"
                });
            });
    }

    const deleteStepResearch = async (id) => {

        swal(
            `¿Eliminar el paso de forma permanente?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        )
            .then(async willDelete => {

                if (willDelete) {
                    setLoading(true)
                    await deleteStepResearchService(id);
                    await getGroupResearch();
                    setLoading(false)
                    swal({
                        text: `Paso eliminado correctamente`,
                        icon: "success"
                    });
                }

            })
        // .catch(err => {
        //     swal({
        //         text: err,
        //         icon: "error"
        //     });
        // });
    }


    return (
        <>
            {
                loading && <SpinnerOrbits />
            }
            <ResourceScreen
                routes={routes}
                researchID={researchID}
                idGroupResearch={groupResearchState.id}
            />
            {
                history.location.pathname === `/ui/experimentalDesign/researchID/${researchID}/groupId/${idGroup}` &&
                !loading &&
                <div className="row">
                    <HeaderExperimental researchState={researchState} />

                    <div className="col s12 m8 offset-m2 mt40">
                        <ul className="collection with-header">
                            <li className="collection-header">
                                <div>
                                    Lista de pasos
                                <a
                                        className="secondary-content cursor-pointer"
                                        onClick={() => addStepResearch()}
                                    >
                                        <i className="material-icons">add</i>
                                    </a>
                                </div>
                            </li>
                            {
                                groupResearchState.step_researches.map(step =>
                                    <StepResearchItem
                                        stepResearch={step}
                                        deleteStepResearch={deleteStepResearch}
                                    />
                                )
                            }
                        </ul>
                    </div>
                </div>
            }
        </>
    )
}
