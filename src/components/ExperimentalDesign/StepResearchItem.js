import React from 'react'

export const StepResearchItem = ({ stepResearch, deleteStepResearch }) => {
    return (
        <li className="collection-item">
            <div>
                <span className="blue-grey-text lighten-5">{stepResearch.step_number} - </span>
                {stepResearch.step_description}
                <a
                    href="#!"
                    className="secondary-content"
                    onClick={() => deleteStepResearch(stepResearch.id)}
                >
                    <i className="material-icons red-text">delete</i>
                </a>
            </div>
        </li>
    )
}
