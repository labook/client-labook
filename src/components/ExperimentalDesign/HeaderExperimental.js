import React, { useEffect } from 'react';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';

export const HeaderExperimental = ({ researchState }) => {
    useEffect(() => {
        if (typeof window.orientation !== "undefined") {
            document.getElementById('enlaceDescargarPdf').click();
            window.close();
        }
    }, [])

    useEffect(() => {
        materializeJs.collapsible();
    })

    return (
        <>
            <div className="col s12 m6 offset-m3 mt40">
                <ul className="collection with-header">
                    <li className="collection-header">
                        <center>
                            <h6>{researchState.name}</h6>
                        </center>
                    </li>
                    <li className="collection-item">
                        <div>
                            {researchState.description}
                        </div>
                    </li>
                </ul>
                <ul class="collapsible">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">picture_as_pdf</i> Desplegar PDF</div>
                        <div class="collapsible-body">
                            {
                                researchState.statements && researchState.statements.length > 0 ?
                                    <div style={{ height: '500px' }}>
                                        <object
                                            data={researchState.statements[0].statement_pdf}
                                            type="application/pdf"
                                            width="100%"
                                            height="100%"
                                        >
                                            <br />
                                            <a href={researchState.statements[0].statement_pdf} id="enlaceDescargarPdf"
                                                download="ReactJS.pdf"
                                            >Tu dispositivo no puede visualizar los PDF, da click aquí para descargarlo</a>
                                        </object>
                                    </div>
                                    :
                                    <center>No hay pdf disponible</center>
                            }
                        </div>
                    </li>
                </ul>
            </div>
        </>
    )
}
