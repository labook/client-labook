import React, { useEffect, useState } from 'react';
import { useForm } from '../../hooks/useForm';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';
import { getCommissions, postCommissions } from '../../utils/services/CommissionService';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
import { getSubjects } from '../../utils/services/SubjectService';
import swal from 'sweetalert';

export const AddCommissionForm = () => {

    const [subjects, setSubjects] = useState([]);
    const [loading, setloading] = useState(false);
    const [error, setError] = useState(null);
    const [subjectSelected, setsubjectSelected] = useState({});
    const [inputValues, handleInputChange, resetForm, handleCheckedChange] = useForm({
        description: '',
        start_time: '',
        end_time: '',
        days: '',
        start_date: '',
        end_date: '',
        id_subject_fk: '',
        active: false
    });

    useEffect(() => {
        gettingSubjects();
        materializeJs.selectOption();
    }, []);

    const gettingSubjects = async () => {
        setloading(true);
        const { data } = await getSubjects();
        setSubjects(data);
        setloading(false);
    }

    const isValidInputs = () => {
        const { description, start_time, end_time, days, start_date, end_date, id_subject_fk } = inputValues;

        if (
            description.length === 0 && start_time.length === 0 &&
            end_time.length === 0 && days.length === 0 &&
            start_date.length === 0 && end_date.length === 0 &&
            id_subject_fk.length === 0
        ) {
            setError({
                description: 'Descripcion requerida',
                start_time: 'Hora inicio de cursada requerido',
                end_time: 'Hora fin de cursada requerido',
                days: 'Dias de cursada requqerido',
                start_date: 'Fecha de inicio requerido',
                end_date: 'Fecha fin requerido',
                id_subject_fk: 'Debe seleccionar una materia',
            });
            return false;
        }
        return true;
    }


    const addCommissions = async (newCommission) => {
        try {
            setloading(true);
            if (isValidInputs()) {
                await postCommissions({
                    ...newCommission,
                    id_subject_fk: subjectSelected.id
                });
                resetForm();
                setsubjectSelected({})
                setloading(false);
            }
            setloading(false);
        }
        catch ({ response }) {
            setloading(false);
            swal({
                title: "Error",
                text: response.data.message,
                icon: "error",
            });
        }

    }


    return (
        <div>
            {
                loading && <SpinnerOrbits />
            }

            {
                !loading &&

                <div>
                    <center>
                        <h5 className="mt30">Ingresar una nueva comisión</h5>
                    </center>

                    <div className="row">
                        <div className="col m6 s12 offset-m3 mt30 mx20">
                            <div class="input-field">
                                <input
                                    autoFocus
                                    autoComplete="off"
                                    className="validate"
                                    name="description"
                                    onChange={handleInputChange}
                                    placeholder="Descripcion"
                                    type="text"
                                    value={inputValues.description}
                                />
                                {error && <p className="red-text fsize12">{error.description}</p>}
                            </div>
                            <div class="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="start_time"
                                    onChange={handleInputChange}
                                    placeholder="Hora de inicio de la cursada ej. 18:00:00"
                                    type="text"
                                    value={inputValues.start_time}
                                />
                                {error && <p className="red-text fsize12">{error.start_time}</p>}
                            </div>
                            <div class="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="end_time"
                                    onChange={handleInputChange}
                                    placeholder="Hora de final de la cursada ej. 22:00:00"
                                    type="text"
                                    value={inputValues.end_time}
                                />
                                {error && <p className="red-text fsize12">{error.end_time}</p>}
                            </div>

                            <div class="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="days"
                                    onChange={handleInputChange}
                                    placeholder="Dias de cursada"
                                    type="text"
                                    value={inputValues.days}
                                />
                                {error && <p className="red-text fsize12">{error.days}</p>}
                            </div>
                            <div class="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="start_date"
                                    onChange={handleInputChange}
                                    placeholder="Fecha de inicio de cursada ej. 2019-07-10"
                                    type="text"
                                    value={inputValues.start_date}
                                />
                                {error && <p className="red-text fsize12">{error.start_date}</p>}
                            </div>

                            <div class="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="end_date"
                                    onChange={handleInputChange}
                                    placeholder="Fecha de fin de cursada ej. 2019-11-29"
                                    type="text"
                                    value={inputValues.end_date}
                                />
                                {error && <p className="red-text fsize12">{error.end_date}</p>}
                            </div>

                            <div className="row">
                                <div className="input-field col s9">
                                    <ul className="collection">
                                        {
                                            subjects.map(subject =>
                                                <li
                                                    key={subject.id}
                                                    className={subjectSelected.id === subject.id ? "collection-item purple lighten-2" : "collection-item"}
                                                    onClick={() => setsubjectSelected(subject)}
                                                >
                                                    {subject.name}
                                                </li>


                                            )
                                        }
                                    </ul>

                                    {/* <select
                                        name="commmissionSelect"
                                        onChange={handleInputChange}
                                        value={inputValues.id_subject_fk}

                                    >
                                        <option value="" disabled selected>Choose your option</option>
                                        {
                                            commissions.map(commission =>
                                                <option
                                                    key={commission.id}
                                                    value={commission.id}
                                                >
                                                    {commission.description}
                                                </option>

                                            )
                                        }
                                    </select> */}
                                    {error && <p className="red-text fsize12">{error.id_subject_fk}</p>}
                                </div>

                                <div class="input-field col s3">
                                    <label>
                                        <input
                                            type="checkbox"
                                            className="filled-in"
                                            name="active"
                                            checked={inputValues.active}
                                            onChange={handleCheckedChange}
                                        />
                                        <span>{inputValues.active ? 'Activo' : 'Inactivo'}</span>
                                    </label>
                                </div>
                            </div>

                            <div className="mt30">
                                <a
                                    className="btn-flat white-text cyan darken-3 waves-effect waves-light btn-small right mt20 mb50"
                                    onClick={() => addCommissions(inputValues)}
                                >
                                    Agregar Materia
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div >
    )
}
