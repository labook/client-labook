import React, { useState, useEffect } from 'react';
import { useForm } from '../../hooks/useForm';
import { useParams, Link } from 'react-router-dom';
import { getCommissionService, putCommissions } from '../../utils/services/CommissionService';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';

export const UpdateCommssionForm = (props) => {

    const { id } = useParams();
    const [loading, setLoading] = useState(false);
    const [commission, setCommission] = useState({});

    const [inputValues, handleInputChange, resetForm, handleCheckedChange, handleInputChangeFile, setInputsValues] = useForm({
        description: '',
        start_time: '',
        end_time: '',
        days: '',
        start_date: '',
        end_date: '',
        id_subject_fk: '',
        active: false
    });

    useEffect(() => {
        getCommissionByID();
        materializeJs.tooltip();
    }, []);

    const getCommissionByID = async () => {
        setLoading(true);
        const { data } = await getCommissionService(id);
        setInputsValues({
            description: data.description,
            start_time: data.start_time,
            end_time: data.end_time,
            days: data.days,
            start_date: data.start_date,
            end_date: data.end_date,
            id_subject_fk: data.id_subject_fk,
            active: data.active
        })
        setCommission(data);
        setLoading(false);
    }

    const updateCommissions = async (newCommission) => {
        const putcomm = await putCommissions(
            commission.id,
            {
                ...newCommission,
                id_subject_fk: commission.subject.id
            });
    }

    return (
        <>
            {
                loading && <SpinnerOrbits />
            }
            {
                !loading &&
                <div>
                    <div className="row mb50">
                        <div className="col s1">
                            <Link
                                className="btn-floating purple lighten-2 m10 p5"

                                to={'/ui/commissions'}
                            >
                                <i
                                    className="material-icons tooltipped"
                                    data-tooltip="Volver a comisiones"
                                >keyboard_backspace</i>
                            </Link>

                        </div>
                        <div className="col s11">
                            <center>
                                <h5>Actualizar {commission && commission.description}</h5>
                            </center>
                        </div>

                    </div>

                    <div className="row">
                        <div className="col m6 s12 offset-m3 mt30">
                            <div className="input-field">
                                <input
                                    autoFocus
                                    autoComplete="off"
                                    className="validate"
                                    name="description"
                                    onChange={handleInputChange}
                                    placeholder="Descripcion"
                                    type="text"
                                    value={inputValues.description}
                                />

                            </div>
                            <div className="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="start_time"
                                    onChange={handleInputChange}
                                    placeholder="Hora de inicio de la cursada"
                                    type="text"
                                    value={inputValues.start_time}
                                />

                            </div>
                            <div className="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="end_time"
                                    onChange={handleInputChange}
                                    placeholder="Hora de final de la cursada"
                                    type="text"
                                    value={inputValues.end_time}
                                />

                            </div>

                            <div className="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="days"
                                    onChange={handleInputChange}
                                    placeholder="Dias de cursada"
                                    type="text"
                                    value={inputValues.days}
                                />

                            </div>
                            <div className="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="start_date"
                                    onChange={handleInputChange}
                                    placeholder="Fecha de inicio de cursada"
                                    type="text"
                                    value={inputValues.start_date}
                                />
                            </div>

                            <div className="input-field">
                                <input
                                    autoComplete="off"
                                    className="validate"
                                    name="end_date"
                                    onChange={handleInputChange}
                                    placeholder="Fehca de fin de cursada"
                                    type="text"
                                    value={inputValues.end_date}
                                />
                            </div>


                            <div className="input-field col s3">
                                <label>
                                    <input
                                        type="checkbox"
                                        className="filled-in"
                                        name="active"
                                        checked={inputValues.active}
                                        onChange={handleCheckedChange}
                                    />
                                    <span>{inputValues.active ? 'Activo' : 'Desactivo'}</span>
                                </label>
                            </div>

                            <div>
                                <a
                                    className="btn-flat white-text cyan darken-3 waves-effect waves-light btn-small right mt20 mb50"
                                    onClick={() => updateCommissions(inputValues)}
                                >
                                    Enviar
                    </a>
                            </div>

                        </div>
                    </div>
                </div>

            }
        </>
    )
}
