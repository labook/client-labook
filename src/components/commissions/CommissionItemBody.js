import React from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { isAuthenticated } from '../../utils/services/auth/authService';
import { createTeacheComissionservice } from '../../utils/services/CommissionService';
import { getTeachersService } from '../../utils/services/TeacherService';

export const CommissionItemBody = ({ commission, getComissions }) => {
    const { rol } = isAuthenticated();

    const addTeacherComission = async () => {
        const { data } = await getTeachersService()
        let options = {};
        data.map(teacher => {
            options[teacher.id] = teacher.name;
        })

        const { value: teacher } = await Swal.fire({
            title: 'Agregar docentes a la comision',
            input: 'select',
            inputOptions: options,
            inputPlaceholder: 'Seleccionar docente',
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value === '') {
                        resolve('Debe seleccionar un docente')
                    }
                    else {
                        resolve()
                    }
                })
            }
        })

        await createTeacheComissionservice(teacher, commission.id)
        getComissions();
        Swal.fire({
            icon: 'success',
            title: 'Se agrego correctamente',
            showConfirmButton: false,
            timer: 1500
        })
    }

    return (
        <div className="collapsible-body teal lighten-5">
            <div className="row">
                <div className="col s11">
                    <div>
                        <span><strong className="mr10">Fecha de inicio del cuatrimestre</strong>{commission.start_date}</span>
                    </div>

                    <div className="mt10">
                        <span><strong className="mr10">Fecha de cierre del cuatrimestre</strong>{commission.end_date}</span>
                    </div>

                    <div className="mt10">
                        <span><strong className="mr10">Hora de inicio de la cursada</strong>{commission.start_time}</span>
                    </div>

                    <div className="mt10">
                        <span><strong className="mr10">Hora de cierre de la cursada</strong>{commission.end_time}</span>
                    </div>

                    <div className="mt10">
                        <span><strong className="mr10">Estado de la cursada</strong>{commission.active ? "Activo" : "Inactivo"}</span>
                    </div>

                    <div className="mt10">
                        <span><strong className="mr10">Materia asociada a la cursada</strong>{commission.subject.name}</span>
                    </div>

                    <div className="mt10">
                        <strong className="mr10">Docentes asociados a la cursada</strong>
                        {
                            commission.teacher_comissions.length > 0 ?
                                commission.teacher_comissions.map(teacher =>
                                    <>
                                        <br /><span>{teacher.teacher.name}</span><br />
                                    </>
                                )
                                : <p className="ml20">Esta comision aun no tiene docentes asignados</p>
                        }
                    </div>
                </div>

                {
                    rol.rol_name === 'admin' &&
                    <div className="col s1">
                        <Link to={`/ui/commissions/form-update/${commission.id}`} className="btn-floating waves-effect waves-light teal darken-4">
                            <i className="material-icons">edit</i>
                        </Link>
                    </div>
                }

                <div className="col s12">
                    <div className="mt10">
                        <Link to={`/ui/commissionGroups/${commission.id}`} className="mr10 right mt8">VER GRUPOS</Link>

                        {
                            rol.rol_name === 'admin' &&
                            <a
                                className="waves-effect waves-light btn btn-small right mr20"
                                onClick={() => addTeacherComission()}
                            >
                                Asignar docente
                        </a>
                        }
                    </div>
                </div>
            </div>

        </div >
    )
}
