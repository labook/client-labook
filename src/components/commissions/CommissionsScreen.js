import React, { useEffect, useState } from 'react';
import './commissions.css';
import { getCommissions } from '../../utils/services/CommissionService';
import { CommissionsList } from './CommissionsList';
import { SpinnerOrbits } from '../UI/spinnerOrbits/SpinnerOrbits';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';
import { Link } from 'react-router-dom';
import { isAuthenticated } from '../../utils/services/auth/authService';

export const CommissionsScreen = () => {

    const [commissions, setCommissions] = useState([]);
    const [loading, setloading] = useState(false);
    const { id_teacher_fk, rol } = isAuthenticated();

    useEffect(() => {
        getComissions();
        materializeJs.selectOption();
    }, []);

    const getComissions = async () => {
        setloading(true);
        const { data } = await getCommissions();
        console.log(data);
        console.log(rol.rol_name)
        let com = data.filter(com => com.teacher_comissions.filter(t => t.id_teacher_fk === id_teacher_fk).length != 0)

        rol.rol_name === 'teacher' && setCommissions(com);
        rol.rol_name === 'admin' && setCommissions(data);
        setloading(false);
    }

    return (
        <div>
            {
                !loading &&
                <div>
                    <div className="row mt30">
                        <div className="col m3 offset-m2">
                            {
                                rol.rol_name === 'admin' &&
                                <Link
                                    className="btn-floating waves-effect waves-light cyan darken-4 mt15"
                                    to={'/ui/commissions/form-newcomission'}
                                >
                                    <i className="material-icons">add</i>
                                </Link>
                            }
                        </div>
                        <div className="col m7">
                            {
                                commissions.length === 0 ?
                                    <div>
                                        <h5>No hay comisiones para mostrar</h5>
                                        <span className="ml100">Crear una nueva comision</span>
                                    </div>
                                    :

                                    <h5>{
                                        rol.rol_name === 'teacher' ? 'Mis comisiones' :
                                            rol.rol_name === 'admin' && 'Comisiones Totales'
                                    }</h5>
                            }
                        </div>
                    </div>
                    <div className="row mt50">
                        <div className="col m8 offset-m2">
                            {
                                commissions.length > 0 &&
                                <CommissionsList
                                    commissions={commissions}
                                    getComissions={getComissions}
                                    setloading={setloading}
                                />
                            }
                        </div>
                    </div>
                </div>
            }

            {
                loading && <SpinnerOrbits />
            }
        </div>
    )
}
