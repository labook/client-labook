import React, { useEffect } from 'react';
import { materializeJs } from '../../utils/MaterializeJS/Materialize.service';
import { CommissionItemBody } from './CommissionItemBody';
import { removeCommission } from '../../utils/services/CommissionService';
import swal from 'sweetalert';
import { isAuthenticated } from '../../utils/services/auth/authService';

export const CommissionsList = ({ commissions, getComissions, setloading }) => {

    const { rol } = isAuthenticated();
    
    useEffect(() => {
        materializeJs.collapsible();
        materializeJs.tooltip();
    }, []);

    const deleteCommission = async (id, description) => {

        swal(
            `¿Eliminar la materia '${description}' de forma permanente?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        )
            .then(async willDelete => {

                if (willDelete) {
                    setloading(true)
                    await removeCommission(id);
                    await getComissions();
                    setloading(false)
                    swal({
                        text: `${description} se eliminó correctamente`,
                        icon: "success"
                    });
                }

            })
            .catch(err => {
                swal({
                    text: err,
                    icon: "error"
                });
            });
    }

    return (
        <ul className="collapsible popout">
            {
                commissions.map(commission =>
                    <li key={commission.id} className="collection">
                        <div className="collapsible-header commission__list-header">

                            <span className="m0 p0 ">
                                <i className="material-icons black-text">expand_more</i>
                                {commission.description} - {commission.subject.name}
                            </span>

                            {
                                rol.rol_name === 'admin' &&
                                <i
                                    className="material-icons red-text tooltipped"
                                    onClick={() => deleteCommission(commission.id, commission.description)}
                                    data-tooltip="Eliminar comision"
                                >
                                    close
                            </i>
                            }

                        </div>
                        <CommissionItemBody commission={commission} getComissions={getComissions} />
                    </li >
                )
            }
        </ul >
    )
}

