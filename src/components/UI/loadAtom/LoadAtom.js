import React from 'react';
import './loadAtom.css';
export const LoadAtom = () => {
    return (
        <div className="container">
            <div className="loader"></div>
            <div className="ring1"></div>
            <div className="ring2"></div>
            <div className="ring3"></div>
        </div>
    )
}
