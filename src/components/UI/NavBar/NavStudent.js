import React from 'react';
import { NavLink } from 'react-router-dom';
import { isAuthenticated } from '../../../utils/services/auth/authService';

export const NavStudent = () => {
  const { rol, id_student_fk } = isAuthenticated();

  return (
    <>
      {
        rol.rol_name === 'student' &&
        <li className="tab">
          <NavLink
            extact
            to={`/ui/student/researchs/${id_student_fk}`}
            activeClassName={"purple active"}
          >
            Investigaciones
          </NavLink>
        </li>
      }
    </>
  )
}
