import React from 'react'
import { NavLink } from 'react-router-dom';
import { isAuthenticated } from '../../../utils/services/auth/authService';

export const NavAdmin = () => {
    const { rol } = isAuthenticated();

    return (
        <>
            {
                rol.rol_name === 'admin' &&
                <li className="tab">
                    <NavLink
                        extact
                        to={"/ui/researchs"}
                        activeClassName={"purple active"}
                    >
                        Investigaciones
                    </NavLink>
                </li>
            }

            {
                rol.rol_name === 'admin' &&
                <li className="tab">
                    <NavLink
                        extact
                        to={"/ui/commissions"}
                        activeClassName={"purple active"}
                    >
                        Comisiones
                    </NavLink>
                </li>
            }

            {
                rol.rol_name === 'admin' &&
                <li className="tab">
                    <NavLink
                        extact
                        to={"/ui/subjects"}
                        activeClassName={"purple active"}
                    >
                        Materias
                    </NavLink>
                </li>
            }
        </>
    )
}
