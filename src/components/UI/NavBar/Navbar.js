import React, { Fragment } from "react";
// Redux
import { connect } from "react-redux";
import { activedDarkMode } from "../../../redux/actions/darkMode.actions";
import { NavStudent } from "./NavStudent";
import { NavTeacher } from "./NavTeacher";
import { NavAdmin } from "./NavAdmin";
import { signoutService } from '../../../utils/services/auth/authService';
import { useHistory } from "react-router-dom";

function Navbar({ darkMode, activedDarkMode }) {

  const history = useHistory();

  const signout = async () => {
    signoutService();
    history.push('/login');
  }

  return (
    <Fragment>
      <nav className={darkMode ? "grey darken-3" : "purple lighten-1"}>
        <div className="nav-wrapper">
          <div data-target="slide-out" className="sidenav-trigger">
            <i className="material-icons cursor-pointer">menu</i>
          </div>
          <div className="brand-logo center">
            LaBook
          </div>
          <div className="right mr20" onClick={() => signout()}>
            <i className="material-icons cursor-pointer">power_settings_new</i>
          </div>
        </div>
      </nav>

      <header className={darkMode ? "grey darken-3" : "purple lighten-1"}>
        <div className="nav-content">
          <ul className="tabs tabs-transparent">

            <NavStudent />

            <NavTeacher />

            <NavAdmin />

            {/* <li
              className="right mr20 pt15 white-text"
            onClick={() => activedDarkMode()}
            >
              <i className="material-icons cursor-pointer">brightness_6</i>
            </li> */}
          </ul>
        </div>
      </header>
    </Fragment>
  );
}

const mapStateToProps = (state) => {
  return {
    darkMode: state.darkMode,
    user: state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    activedDarkMode: () => dispatch(activedDarkMode())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
