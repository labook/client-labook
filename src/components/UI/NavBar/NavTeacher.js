import React from 'react';
import { NavLink } from 'react-router-dom';
import { isAuthenticated } from '../../../utils/services/auth/authService';


export const NavTeacher = () => {
    const { rol } = isAuthenticated();
    return (
        <>
           
            {
                rol.rol_name === 'teacher' &&
                <li className="tab">
                    <NavLink
                        extact
                        to={"/ui/commissions"}
                        activeClassName={"purple active"}
                    >
                        Comisiones
                    </NavLink>
                </li>
            }
            {
                rol.rol_name === 'teacher' &&
                <li className="tab">
                    <NavLink
                        extact
                        to={"/ui/resources"}
                        activeClassName={"purple active"}
                    >
                        Catalogo de recursos
                    </NavLink>
                </li>
            }
            {
                rol.rol_name === 'teacher' &&
                <li className="tab">
                    <NavLink
                        extact
                        to={"/ui/researchs"}
                        activeClassName={"purple active"}
                    >
                        Investigaciones
                    </NavLink>
                </li>
            }
        </>
    )
}
