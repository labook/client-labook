import React from 'react';
import './loading.css';

export const Loading = () => {
    return (
        <div className="loader">
            <div className="cube__one"></div>
            <div className="cube__two"></div>
            <div className="cube__three"></div>
        </div>
    )
}
