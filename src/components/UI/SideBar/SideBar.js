import React, { Component } from "react";
import logo_unahur from "../../../assets/img/png/logo_unahur.png";
import user_avatar from "../../../assets/jpg/user.jpg";
import { materializeJs } from "../../../utils/MaterializeJS/Materialize.service";
import { connect } from "react-redux";
import { activedDarkMode } from "../../../redux/actions/darkMode.actions";
class SideBar extends Component {

  componentDidMount() {
    materializeJs.collapsible();
  }

  render() {
    const { darkMode } = this.props;
    return (
      <>
        <ul id="slide-out" className={darkMode ? "sidenav collapsible grey darken-3 border-none" : "sidenav collapsible grey lighten-5"}>
          <li>
            <div className="user-view">
              <div className="background pt20 ml10 mr10">
                <img src={logo_unahur} className="responsive-img" />
              </div>
              <div className="mt40">
                <a href="#user" className="center-align">
                  <img className="circle border-none" src={user_avatar} />
                </a>
              </div>

              <a href="#name">
                <span className={darkMode ? "white-text name" : "black-text name"}>Jono</span>
              </a>
              <a href="#email">
                <span className={darkMode ? "white-text email" : "black-text email"}>Jono@gmail.com</span>
              </a>
            </div>
          </li>
          {/* <li>
          <div className="collapsible-header">
            Proyectos
            <div className="ml150">
              <i className="material-icons">keyboard_arrow_down</i>
            </div>
          </div>
          <ul className="collapsible-body collection">
            <li className="collection-item">
              <span>Medicion de propiedades fisicas de la materia</span>
            </li>
            <li className="collection-item">
              <span>Mediciones e incertezas</span>
            </li>
          </ul>
        </li> */}
          <li>
            <ul className="mb0">
              <li>
                <a
                  className={darkMode ? "white-text waves-effect" : "waves-effect"}
                // onClick={() => this.props.activedDarkMode()}
                >
                  {darkMode ? "Desactivar" : "Activar"} Modo Oscuro
            <i className={darkMode ? "white-text material-icons right" : "material-icons right"}>brightness_6</i>
                </a>
              </li>
              <li>
                <a className={darkMode ? "white-text waves-effect" : "waves-effect"} href="#!">
                  Cerrar sesión
            <i className={darkMode ? "white-text material-icons right" : "material-icons right"}>power_settings_new</i>
                </a>
              </li>
            </ul>
          </li>
        </ul >

      </>

    );
  }
}

{
  /* <li>
          <a class="sidenav-close" href="#!">
            Clicking this will close Sidenav
          </a>
        </li> */
}
const mapStateToProps = (state) => {
  return {
    darkMode: state.darkMode,
    user: state.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    activedDarkMode: () => dispatch(activedDarkMode())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar)