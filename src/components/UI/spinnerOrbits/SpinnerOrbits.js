import React from 'react';
import './spinnerOrbits.css';
export const SpinnerOrbits = () => {
    return (
        <div className="spinner-box">
            <div className="blue-orbit leo">
            </div>

            <div className="green-orbit leo">
            </div>

            <div className="red-orbit leo">
            </div>

            <div className="black-orbit w1 leo">
            </div><div className="black-orbit w2 leo">
            </div><div className="black-orbit w3 leo">
            </div>
        </div>

    )
}
