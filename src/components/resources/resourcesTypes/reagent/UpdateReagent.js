import React, { useEffect, useState } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom';
import swal from 'sweetalert';
import { useForm } from '../../../../hooks/useForm';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { reagentService } from '../../../../utils/services/ReagentService';
import { ResourcesForm } from '../ResourcesForm';

export const UpdateReagent = () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [reagentState, setreagentState] = useState({});
    const history = useHistory();
    const { id: idReagent } = useParams();
    const [inputValues, handleInputChange, resetForm, handleCheckedChange, handleInputChangeFile, setInputsValues] = useForm({
        name: '',
        cas: '',
        description: '',
        image: '',
    });

    useEffect(() => {
        getReagentByID(idReagent);
    }, [])

    const getReagentByID = async (id) => {
        setLoading(true);
        const { data } = await reagentService.getReagentIDService(id);
        setInputsValues({
            name: data.name,
            cas: data.cas,
            description: data.description,
            image: data.image,
        });
        setreagentState(data);
        setLoading(false);
    }


    const updateReagent = async (id, newReagent) => {
        try {
            setLoading(true);
            // const errors = inputValidate();

            // if (Object.keys(errors).length > 0) {
            // swal({
            // text: "Todos los campos son obligatorios",
            // icon: "error",
            // });
            setLoading(false);
            setError(true);
            // } else {
            await reagentService.updateReagentService(id, { ...newReagent });
            setLoading(false);
            setError(false);
            swal({
                title: newReagent.name,
                text: "Creado correctamente",
                icon: "success",
            }).then(willacept => {
                if (willacept) {
                    history.push('/ui/resources/reagents');
                    resetForm();
                };
            });
            // }
        }
        catch (e) {
            setLoading(false);
            console.log(e)
            swal({
                title: "Error",
                text: "Error al actualizar el reactivo",
                icon: "error",
            });
        }
    }
    return (
        <>
            {
                loading &&
                <SpinnerOrbits />
            }

            <div className="row">
                <div className="col m5 s12 mt30">
                    <Link
                        to={"/ui/resources/reagents"}
                        className="breadcrumb cyan-text darken-4"
                    >
                        Catalogo
                </Link>
                    <Link className="breadcrumb cyan-text darken-4">Editar Reactivo</Link>
                </div>
            </div>
            <center><h1>Editar {reagentState.name}</h1></center>
            <div className="row">
                <ResourcesForm
                    handleInputChange={handleInputChange}
                    inputValues={inputValues}
                    handleInputChangeFile={handleInputChangeFile}
                    cas={true}
                    onUpdateResource={updateReagent}
                    idUpdate={idReagent}
                // inputValidate={inputValidate}
                // error={error}
                />
            </div>
        </>
    )
}
