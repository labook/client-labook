import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom';
import swal from 'sweetalert';
import { useForm } from '../../../../hooks/useForm';
import { reagentService } from '../../../../utils/services/ReagentService';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { ResourcesForm } from '../ResourcesForm';

export const AddReagent = () => {
    const [loading, setLoading] = useState(false);
    const history = useHistory();
    const [error, setError] = useState(false);
    const [inputValues, handleInputChange, resetForm, handleCheckedChange, handleInputChangeFile, inputValidate] = useForm({
        name: '',
        cas: '',
        description: '',
        image: '',
        cant: 0
    });

    const isValidInputs = () => {
        const { name, cas, description, image } = inputValues;
        if (name.length === 0 && cas.length === 0 && description.length === 0) {
            setError({
                name: 'Nombre requerido',
                cas: 'CAS requerido',
                description: 'Descripcion requerida',
                // image: 'Imagen requerida'
            });
            return false;
        }
        return true;
    }

    const addReagent = async (newReagent) => {
        try {
            setLoading(true);
            if (isValidInputs()) {
                await reagentService.postReagentService({ ...newReagent });
                setLoading(false);
                setError(false);
                swal({
                    title: newReagent.name,
                    text: "Creado correctamente",
                    icon: "success",
                }).then(willacept => {
                    if (willacept) {
                        history.push('/ui/resources/reagents');
                        resetForm();
                    };
                });
            }
            setLoading(false);
        }
        catch ({ response }) {
            setLoading(false);
            if (response) {
                swal({
                    title: "Error",
                    text: response.data.message,
                    icon: "error",
                });
            }
        }
    }

    return (
        <>
            {
                loading &&
                <SpinnerOrbits />
            }

            <div className="row">
                <div className="col m5 s12 mt30">
                    <Link
                        to={"/ui/resources/reagents"}
                        className="breadcrumb cyan-text darken-4"
                    >
                        Catalogo
                </Link>
                    <Link className="breadcrumb cyan-text darken-4">Crear Reactivo</Link>

                </div>
            </div>
            <center><h1>Agregar nuevo Reactivo</h1></center>
            <div className="row">
                <ResourcesForm
                    handleInputChange={handleInputChange}
                    inputValues={inputValues}
                    handleInputChangeFile={handleInputChangeFile}
                    addResource={addReagent}
                    cas={true}
                    error={error}
                />
            </div>
        </>
    )
}
