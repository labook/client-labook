import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import swal from 'sweetalert';
import { isAuthenticated } from '../../../../utils/services/auth/authService';
import { reagentService } from '../../../../utils/services/ReagentService';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { ItemResource } from '../ItemResource';

export const ResourceReagent = () => {
    const { rol } = isAuthenticated();
    const [loading, setloading] = useState(false);
    const [reagentState, setreagentState] = useState([]);
    const { idGroupResearch } = useParams();

    useEffect(() => {
        getReagent();
    }, []);

    const getReagent = async () => {
        setloading(true);
        const { data } = await reagentService.getReagentService(1, 100);
        setreagentState(data);
        setloading(false);
    }

    const onDeleteResource = ({ name, id }) => {
        swal(
            `¿Eliminar '${name}' de forma permanente?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        ).then(async willDelete => {
            if (willDelete) {
                setloading(true);
                await reagentService.removeReagentService(id);
                getReagent();
                setloading(false);
            };
        });
    }

    const selectionReagent = async (id_reagent_fk, name, cant) => {
        swal(
            `¿Confirmar la seleccion del material ${name}?`,
            {
                content: "input",
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Aceptar'
                }
            }
        )
            .then(async willConfirm => {
                if (!willConfirm) throw null;
                if (willConfirm) {
                    setloading(true);
                    if (willConfirm < cant) {
                        await reagentService.updateReagentCantService(id_reagent_fk, cant - willConfirm);
                        await reagentService.addReagentSelectionService(id_reagent_fk, Number(idGroupResearch), willConfirm);
                        getReagent();
                        setloading(false);
                        swal({
                            text: `${name} se ha añadido correctamente a la seleccion de recursos`,
                            icon: "success"
                        });
                    }
                    else {
                        setloading(false);
                        swal({
                            text: `Upsi dopsy`,
                            icon: "error"
                        });
                    }
                }

            })
            .catch(err => {
                swal({
                    text: err,
                    icon: "error"
                });
            });
    }

    return (
        <div>
            {
                loading && <SpinnerOrbits />
            }

            {!loading && reagentState.length > 0 ?
                <div>
                    <div className="collection">
                        <a className="collection-item pt35 height100">
                            <span className="badge" data-badge-caption="Reactivos disponibles para su selección">
                                {reagentState.length}
                            </span>
                            Catalogo de reactivos
                        </a>
                    </div>
                    <div className="row mt70">
                        {
                            reagentState.map((resource) => (
                                <ItemResource
                                    key={resource.id}
                                    resource={resource}
                                    onDeleteResource={onDeleteResource}
                                    pathEdit={`/ui/resources/UpdateReagent/${resource.id}`}
                                    selectioResource={selectionReagent}
                                />
                            ))
                        }
                    </div>
                </div>
                : <center className="mt100">No hay Reactivo disponibles</center>
            }

            {
                !loading &&
                rol.rol_name === 'teacher' &&
                <div className="fixed-action-btn">
                    <Link
                        to={'/ui/resources/AddReagent'}
                        className="waves-effect waves-light btn cyan darken-4"
                    >
                        <i className="material-icons right">add</i>Nuevo Reactivo
                    </Link>
                </div>
            }
        </div>
    )
}
