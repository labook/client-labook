import React from 'react';
import { NavLink } from 'react-router-dom';

export const ResourceNavStudent = ({ researchID, idGroupResearch }) => {
    return (
        <>
            <li className={"tab right cursor-pointer"}>
                <NavLink
                    to={`/ui/experimentalDesign/researchID/${researchID}/groupId/${idGroupResearch}/resources/resource-selection`}
                    activeClassName="purple active"
                >
                    Seleccion de recursos
              </NavLink>
            </li>
            <li className={"tab right cursor-pointer"}>
                <NavLink
                    to={`/ui/experimentalDesign/researchID/${researchID}/groupId/${idGroupResearch}/resources/equipments`}
                    activeClassName="purple active"
                >
                    Equipamientos
              </NavLink>
            </li>
            <li className={"tab right cursor-pointer"}>
                <NavLink
                    to={`/ui/experimentalDesign/researchID/${researchID}/groupId/${idGroupResearch}/resources/reagents`}
                    activeClassName="purple active"
                >
                    Reactivos
              </NavLink>
            </li>
            <li className={"tab right cursor-pointer"}>
                <NavLink
                    to={`/ui/experimentalDesign/researchID/${researchID}/groupId/${idGroupResearch}/resources/materials`}
                    activeClassName="purple active"
                >
                    Materiales
              </NavLink>
            </li>
        </>
    )
}
