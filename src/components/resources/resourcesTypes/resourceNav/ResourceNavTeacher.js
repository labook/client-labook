import React from 'react';
import { NavLink } from 'react-router-dom';

export const ResourceNavTeacher = () => {
    return (
        <>
            <li className={"tab right cursor-pointer"}>
                <NavLink
                    to={`/ui/resources/equipments`}
                    activeClassName="purple active"
                >
                    Equipamientos
                    </NavLink>
            </li>
            <li className={"tab right cursor-pointer"}>
                <NavLink
                    to={`/ui/resources/reagents`}
                    activeClassName="purple active"
                >
                    Reactivos
                    </NavLink>
            </li>
            <li className={"tab right cursor-pointer"}>
                <NavLink
                    to={`/ui/resources/materials`}
                    activeClassName="purple active"
                >
                    Materiales
                    </NavLink>
            </li>
        </>
    )
}
