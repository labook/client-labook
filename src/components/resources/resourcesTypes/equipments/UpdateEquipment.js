import React, { useEffect, useState } from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import swal from 'sweetalert';
import { useForm } from '../../../../hooks/useForm';
import { equipmentService } from '../../../../utils/services/EquipmentService';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { ResourcesForm } from '../ResourcesForm';

export const UpdateEquipment = () => {

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [equipmentState, setEquipmentState] = useState({});
    const history = useHistory();
    const { id: idEquipment } = useParams();
    const [inputValues, handleInputChange, resetForm, handleCheckedChange, handleInputChangeFile, setInputsValues] = useForm({
        name: '',
        description: '',
        image: '',
    });

    useEffect(() => {
        getEquipmentByID(idEquipment);
    }, [])

    const getEquipmentByID = async (id) => {
        setLoading(true);
        const { data } = await equipmentService.getEquipmentIDService(id);
        setInputsValues({
            name: data.name,
            description: data.description,
            image: data.image,
        });
        setEquipmentState(data);
        setLoading(false);
    }

    const updateEquipment = async (id, newEquipment) => {
        try {
            setLoading(true);
            // const errors = inputValidate();
            // if (Object.keys(errors).length > 0) {
            // swal({
            //     text: "Todos los campos son obligatorios",
            //     icon: "error",
            // });
            setError(true);
            setLoading(false);
            // } else {
            await equipmentService.updateEquipmentService(id, { ...newEquipment });
            setLoading(false);
            setError(false);
            swal({
                title: newEquipment.name,
                text: "Creado correctamente",
                icon: "success",
            }).then(willacept => {
                if (willacept) {
                    history.push('/ui/resources/equipments');
                    resetForm();
                };
            });
            // }
        }
        catch (e) {
            setLoading(false);
            swal({
                title: "Error",
                text: "Algo ha ocurrido",
                icon: "error",
            });
        }
    }

    return (
        <div>
            {
                loading &&
                <SpinnerOrbits />
            }

            <div className="row">
                <div className="col m5 s12 mt30">
                    <Link
                        to={"/ui/resources/equipments"}
                        className="breadcrumb cyan-text darken-4"
                    >
                        Catalogo
                    </Link>
                    <Link className="breadcrumb cyan-text darken-4">Editar Equipamiento</Link>
                </div>
            </div>
            <center><h1>Editar {equipmentState.name}</h1></center>
            <div className="row">
                <ResourcesForm
                    handleInputChange={handleInputChange}
                    inputValues={inputValues}
                    handleInputChangeFile={handleInputChangeFile}
                    onUpdateResource={updateEquipment}
                    idUpdate={idEquipment}
                // inputValidate={inputValidate}
                // error={error}
                />
            </div>
        </div>
    )
}
