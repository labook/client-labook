import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom';
import swal from 'sweetalert';
import { isAuthenticated } from '../../../../utils/services/auth/authService';
import { equipmentService } from '../../../../utils/services/EquipmentService';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { ItemResource } from '../ItemResource';

export const ResourceEquipment = () => {
    const { rol } = isAuthenticated();
    const [loading, setloading] = useState(false);
    const [equipmentState, setEquipmentState] = useState([]);
    const { idGroupResearch } = useParams();


    useEffect(() => {
        getEquipment();
    }, []);

    const getEquipment = async () => {
        setloading(true);
        const { data } = await equipmentService.getEquipmentService(1, 100);
        setEquipmentState(data);
        setloading(false);
    }

    const onDeleteResource = ({ name, id }) => {
        swal(
            `¿Eliminar '${name}' de forma permanente?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        ).then(async willDelete => {
            if (willDelete) {
                setloading(true);
                await equipmentService.removeEquipmentService(id);
                getEquipment();
                setloading(false);
            };
        });
    }

    const selectionEquipment = async (id_equipment_fk, name, cant) => {
        swal(
            `¿Confirmar la seleccion del equipamiento ${name}?`,
            {
                content: "input",
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Aceptar'
                }
            }
        )
            .then(async willConfirm => {
                if (!willConfirm) throw null;
                if (willConfirm) {
                    setloading(true);
                    if(willConfirm < cant){
                        await equipmentService.updateEquipmentCantService(id_equipment_fk, cant - willConfirm);
                        await equipmentService.addEquipmentSelectionService(id_equipment_fk, Number(idGroupResearch), willConfirm);
                        getEquipment()
                        setloading(false);
                        swal({
                            text: `Se ha añadido correctamente a la seleccion de recursos`,
                            icon: "success"
                        });
                    }
                    else{
                        setloading(false);
                        swal({
                            text: `Upsi dopsy`,
                            icon: "error"
                        });
                    }
                }

            })
            .catch(err => {
                swal({
                    text: err,
                    icon: "error"
                });
            });
    }

    return (
        <div>
            {
                loading && <SpinnerOrbits />
            }

            {!loading && equipmentState.length > 0 ?

                <div>
                    <div className="collection">
                        <a className="collection-item pt35 height100">
                            <span className="badge" data-badge-caption="Equipos disponibles para su selección">
                                {equipmentState.length}
                            </span>
                            Catalogo de equipos
                        </a>
                    </div>
                    <div className="row mt70">
                        {
                            equipmentState.map(resource => (
                                <ItemResource
                                    key={resource.id}
                                    resource={resource}
                                    onDeleteResource={onDeleteResource}
                                    pathEdit={`/ui/resources/UpdateEquipment/${resource.id}`}
                                    selectioResource={selectionEquipment}
                                />
                            ))
                        }
                    </div>
                </div>
                : <center className="mt100">No hay Equipamiento disponibles</center>
            }

            {
                !loading &&
                rol.rol_name === 'teacher' &&
                <div className="fixed-action-btn">
                    <Link
                        to={'/ui/resources/AddEquipment'}
                        className="waves-effect waves-light btn cyan darken-4"
                    >
                        <i className="material-icons right">add</i>Nuevo Equipamiento
                    </Link>
                </div>
            }
        </div>
    )
}
