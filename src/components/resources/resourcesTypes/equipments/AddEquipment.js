import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import swal from 'sweetalert';
import { useForm } from '../../../../hooks/useForm';
import { equipmentService } from '../../../../utils/services/EquipmentService';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { ResourcesForm } from '../ResourcesForm';

export const AddEquipment = () => {

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const history = useHistory();
    const [inputValues, handleInputChange, resetForm, handleCheckedChange, handleInputChangeFile, inputValidate] = useForm({
        name: '',
        description: '',
        image: '',
    });

    const isValidInputs = () => {
        const { name, description, image } = inputValues;
        if (name.length === 0 && description.length === 0 && image.length === 0) {
            setError({
                name: 'Nombre requerido',
                description: 'Descripcion requerida',
                image: 'Imagen requerida'
            });
            return false;
        }
        return true;
    }

    const addEquipment = async (newEquipment) => {
        try {
            setLoading(true);
            if (isValidInputs()) {
                await equipmentService.postEquipmentService({ ...newEquipment });
                setLoading(false);
                swal({
                    title: newEquipment.name,
                    text: "Creado correctamente",
                    icon: "success",
                }).then(willacept => {
                    if (willacept) {
                        history.push('/ui/resources/equipments');
                        resetForm();
                    };
                });
            }
            setLoading(false);
        }
        catch ({ response }) {
            setLoading(false);
            if (response) {
                swal({
                    title: "Error",
                    text: response.data.message,
                    icon: "error",
                });
            }
        }
    }
    return (
        <>
            {
                loading &&
                <SpinnerOrbits />
            }

            <div className="row">
                <div className="col m5 s12 mt30">
                    <Link
                        to={"/ui/resources/equipments"}
                        className="breadcrumb cyan-text darken-4"
                    >
                        Catalogo
                    </Link>
                    <Link className="breadcrumb cyan-text darken-4">Crear Equipamiento</Link>
                </div>
            </div>
            <center><h1>Agregar nuevo Equipamiento</h1></center>
            <div className="row">
                <ResourcesForm
                    handleInputChange={handleInputChange}
                    inputValues={inputValues}
                    handleInputChangeFile={handleInputChangeFile}
                    addResource={addEquipment}
                    error={error}
                />
            </div>
        </>
    )
}
