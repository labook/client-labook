import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import swal from 'sweetalert';
import { equipmentService } from '../../../../utils/services/EquipmentService';
import { materialService } from '../../../../utils/services/MaterialService';
import { reagentService } from '../../../../utils/services/ReagentService';
import { getGroupResearIDchService, getResearchService } from '../../../../utils/services/ResearchsService';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { SelectionItem } from './SelectionItem';

export const ResourceSelection = () => {

    const { researchID, idGroupResearch } = useParams();
    const [loading, setLoading] = useState(false);
    const [researchState, setResearchState] = useState({});
    const [groupResearchState, setGroupResearchState] = useState({
        material_selections: [],
        equipment_selections: [],
        reagent_selections: []

    });


    useEffect(() => {
        getReseacrhId();
        getGroupResearch();
    }, []);

    const getGroupResearch = async () => {
        setLoading(true);
        try {
            const { data } = await getGroupResearIDchService(idGroupResearch);
            console.log(data)
            setGroupResearchState(data.groupResearch)
        }
        catch (e) {
            console.log(e)
        }
        finally {
            setLoading(false);
        }
    }

    const getReseacrhId = async () => {
        setLoading(true);
        try {
            const { data } = await getResearchService(researchID);
            setResearchState(data);
        }
        catch (e) {
            console.log(e)
        }
        finally {
            setLoading(false);
        }
    }

    const deleteEquipmentSelection = (id, name, cant) => {
        swal(
            `¿Eliminar permanentemente el equipo ${name}?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        )
            .then(async willConfirm => {

                if (willConfirm) {
                    setLoading(true)
                    await equipmentService.deleteEquipmentsSelectionService(id);
                    getGroupResearch()
                    setLoading(false);
                    swal({
                        text: `Se ha eliminado correctamente`,
                        icon: "success"
                    });
                }

            })
    }

    const deleteMaterialSelection = (id, name, resource) => {
        swal(
            `¿Eliminar permanentemente el material ${name}?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
            )
            .then(async willConfirm => {
                
                if (willConfirm) {
                    setLoading(true)
                    await materialService.updateMaterialCantService(resource.id, resource.total);
                    await materialService.deleteMaterialSelectionService(id);
                    getGroupResearch();
                    setLoading(false);
                    swal({
                        text: `Se ha eliminado correctamente`,
                        icon: "success"
                    });
                }

            })
    }

    const deleteReagentSelection = (id, name) => {
        swal(
            `¿Eliminar permanentemente el reactivo ${name}?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        )
            .then(async willConfirm => {

                if (willConfirm) {
                    setLoading(true)
                    await reagentService.deleteReagentSelectionService(id);
                    getGroupResearch();
                    setLoading(false);
                    swal({
                        text: `Se ha eliminado correctamente`,
                        icon: "success"
                    });
                }

            })
    }

    return (
        <>
            {
                loading && <SpinnerOrbits />
            }
            {
                !loading &&
                <ul className="collection">
                    <li className={"collection-item center"}>
                        <h5 className="d-inline">Recursos Seleccionados de <span className="blue-text darken-1 cursor-pointer">{researchState.name}</span></h5>
                        <span className={"d-inline right font-weight-bold"}>
                            Acreditar pedido
                        </span>
                    </li>

                    {
                        groupResearchState.material_selections.map(material =>
                            <SelectionItem
                                resource={material}
                                key={material.id}
                                onDeleteResource={deleteMaterialSelection}
                            />
                        )
                    }

                    {
                        groupResearchState.equipment_selections.map(equipment =>
                            <SelectionItem
                                key={equipment.id}
                                resource={equipment}
                                onDeleteResource={deleteEquipmentSelection}
                            />
                        )
                    }

                    {
                        groupResearchState.reagent_selections.map(reagent =>
                            <SelectionItem
                                resource={reagent}
                                key={reagent.id}
                                onDeleteResource={deleteReagentSelection}
                            />
                        )
                    }

                    {
                        groupResearchState.reagent_selections.length === 0 &&
                        groupResearchState.equipment_selections.length === 0 &&
                        groupResearchState.material_selections.length === 0 &&
                        <li className="collection-item avatar">
                            <center className="pt20">
                                <strong title="title">No hay ningun recurso seleccionado</strong>
                            </center>
                        </li>
                    }
                </ ul>
            }
        </>
    )
}
