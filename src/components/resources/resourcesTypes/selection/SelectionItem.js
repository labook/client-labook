import React from 'react'

export const SelectionItem = ({ resource, onDeleteResource }) => {

    const name = () => {
        return (resource.Material) ?
            resource.Material.name :
            (resource.Reagent) ?
                resource.Reagent.name :
                resource.Equipment.name
    }

    const cant = () => {
        return (resource.Material) ?
            resource.cant :
            (resource.Reagent) ?
                resource.cant :
                resource.cant
    }

    const deletecant = () => {
        return (resource.Material) ?
            {
                total: resource.cant + resource.Material.cant,
                id: resource.Material.id
            } :
            (resource.Reagent) ?
                {
                    total: resource.cant + resource.Reagent.cant,
                    id: resource.Reagent.id
                } :
                {
                    total: resource.cant + resource.Equipment.cant,
                    id: resource.Equipment.id
                }
    }

    const image = () => {
        return (resource.Material) ?
            resource.Material.image :
            (resource.Reagent) ?
                resource.Reagent.image :
                resource.Equipment.image
    }

    const description = () => {
        return (resource.Material) ?
            resource.Material.description :
            (resource.Reagent) ?
                resource.Reagent.description :
                resource.Equipment.description
    }

    const typeResource = () => {
        return (resource.Material) ?
            'Tipo de recurso: Material' :
            (resource.Reagent) ?
                'Tipo de recurso: Reactivo' :
                'Tipo de recurso: Equipamiento'
    }

    return (
        <li className="collection-item avatar">
            <img
                alt={name()}
                className="circle responsive-img"
                style={{ height: "50px", width: "50px" }}
                src={image()}
            />
            <span className="title">{name()}</span>
            <p>{description()}</p>
            <p>{typeResource()}</p>
            <p>Catindad seleccionada: {cant()}</p>
            <a href="#!" className="secondary-content" onClick={() => onDeleteResource(resource.id, name(), deletecant())}>
                <i className="material-icons red-text">delete</i>
            </a>
        </li>
    )
}
