import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { isAuthenticated } from '../../../utils/services/auth/authService';

export const ItemResource = ({ resource, onDeleteResource, pathEdit, selectioResource }) => {
    const [_i, set_i] = useState(0);
    const { rol } = isAuthenticated();

    useEffect(() => {
        resizeImgage(resource.image);
    }, [])

    const resizeImgage = (image) => {
        const img = new Image();
        img.src = image;
        img.onload = (e) => {
            const canvas = document.createElement('canvas');
            const MAX_WIDTH = 400;
            const scaleSize = MAX_WIDTH / e.target.width;
            canvas.width = 450;
            canvas.height = 380;
            const ctx = canvas.getContext('2d');
            ctx.drawImage(e.target, 0, 0, canvas.width, canvas.height);
            const srcEncoded = ctx.canvas.toDataURL(e.target, 'image/jpeg');
            document.querySelector(`#resize${_i}`).src = srcEncoded;
            set_i(_i + 1);
        };
    }

    return (
        <div
            key={resource.id}
            className="col s12 m3 mt50"
        >
            <div className="card">
                <div className="card-image waves-effect waves-block waves-light"> {/*height200 height150 */}
                    {/* <img className="activator img_cover" src={resource.image} alt={resource.id} /> */}
                    <img id={`resize${_i}`} />
                </div>
                <div className="card-content height150">
                    <span className="card-title activator grey-text text-darken-4">{resource.name}<i className="material-icons right">more_vert</i></span>
                    {
                        rol.rol_name === 'teacher' &&
                        <a
                            href="#"
                            className="red-text"
                            onClick={() => onDeleteResource({ ...resource })}
                        >
                            Eliminar
                    </a>
                    }
                    {
                        rol.rol_name === 'teacher' &&
                        <Link to={pathEdit} className="cyan-text darken-4 right">Editar</Link>
                    }
                </div>
                <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">
                        {resource.name}
                        <i className="material-icons right red-text">close</i>
                    </span>
                    <div>
                        <p>{resource.description}</p>
                        {
                            resource.cas &&
                            <span>{resource.cas}</span>
                        }
                        <p>Cantidad disponible: {resource.cant}</p>
                        <br />
                        {
                            rol.rol_name === 'student' &&
                            <a
                                className={resource.cant > 0 ? "waves-effect waves-light btn mt25 purple darken-2": "waves-effect waves-light btn mt25 purple darken-2 disabled"}
                                onClick={() => selectioResource(resource.id, resource.name, resource.cant)}
                            >
                                Agregar a mi seleccion
                            </a>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}
