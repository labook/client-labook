import React, { useEffect, useState } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom';
import swal from 'sweetalert';
import { useForm } from '../../../../hooks/useForm';
import { materialService } from '../../../../utils/services/MaterialService';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { ResourcesForm } from '../ResourcesForm';

export const UpdateMaterial = () => {

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [materialState, setmaterialState] = useState({});
    const history = useHistory();
    const { id: idMaterial } = useParams();
    const [inputValues, handleInputChange, resetForm, handleCheckedChange, handleInputChangeFile, setInputsValues] = useForm({
        name: '',
        description: '',
        image: '',
    });

    useEffect(() => {
        getMaterialtByID(idMaterial);
    }, [])

    const getMaterialtByID = async (id) => {
        setLoading(true);
        const { data } = await materialService.getMaterialIDService(id);
        setInputsValues({
            name: data.name,
            description: data.description,
            image: data.image,
        });
        setmaterialState(data);
        setLoading(false);
    }

    const updateMaterial = async (id, newmaterial) => {
        try {
            setLoading(true);
            // const errors = inputValidate();
            // if (Object.keys(errors).length > 0) {
            // swal({
            // text: "Todos los campos son obligatorios",
            // icon: "error",
            // });
            setError(true);
            setLoading(false);
            // } else {
            console.log(newmaterial)
            await materialService.updateMaterialService(id, { ...newmaterial });
            setLoading(false);
            setError(false);
            swal({
                title: newmaterial.name,
                text: "Creado correctamente",
                icon: "success",
            }).then(willacept => {
                if (willacept) {
                    // history.push('/ui/resources/materials');
                    resetForm();
                };
            });
            // }
        }
        catch (e) {
            setLoading(false);
            swal({
                title: "Error",
                text: "Algo ha ocurrido",
                icon: "error",
            });
        }
    }

    return (
        <>
            {
                loading &&
                <SpinnerOrbits />
            }

            <div className="row">
                <div className="col m5 s12 mt30">
                    <Link
                        // to={"/ui/resources/materials"}
                        className="breadcrumb cyan-text darken-4"
                    >
                        Catalogo
                </Link>
                    <Link className="breadcrumb cyan-text darken-4">Editar Material</Link>
                </div>
            </div>
            <center><h1>Editar {materialState.name}</h1></center>
            <div className="row">
                <ResourcesForm
                    handleInputChange={handleInputChange}
                    inputValues={inputValues}
                    handleInputChangeFile={handleInputChangeFile}
                    onUpdateResource={updateMaterial}
                    idUpdate={idMaterial}
                // inputValidate={inputValidate}
                // error={error}
                />
            </div>
        </>
    )
}
