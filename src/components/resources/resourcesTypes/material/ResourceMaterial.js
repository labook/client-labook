import React, { useEffect, useState } from 'react'
import { materializeJs } from '../../../../utils/MaterializeJS/Materialize.service'
import { materialService } from '../../../../utils/services/MaterialService'
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits'
import { isAuthenticated } from '../../../../utils/services/auth/authService'
import { ItemResource } from '../ItemResource'
import { Link, useParams } from 'react-router-dom';
import swal from 'sweetalert'

export const ResourceMaterial = () => {
    const { rol } = isAuthenticated();
    const { idGroupResearch } = useParams();
    const [materialState, setMaterialState] = useState([]);
    const [loading, setloading] = useState([]);

    useEffect(() => {
        getMaterial();
    }, []);

    useEffect(() => {
        materializeJs.dropDownInit();
    })

    const getMaterial = async () => {
        setloading(true);
        const { data } = await materialService.getMaterialService(1, 15);
        setMaterialState(data);
        setloading(false);
    }

    const onDeleteResource = ({ name, id }) => {
        swal(
            `¿Eliminar '${name}' de forma permanente?`,
            {
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Eliminar'
                }
            }
        ).then(async willDelete => {
            if (willDelete) {
                setloading(true);
                await materialService.removeMaterialService(id);
                getMaterial();
                setloading(false);
            };
        });
    }

    const selectionMaterial = async (id_material_fk, name, cant) => {
        swal(
            `¿Confirmar la seleccion del material ${name}?`,
            {
                content: "input",
                buttons: {
                    cancel: 'Cancelar',
                    confirm: 'Aceptar'
                }
            }
        )
            .then(async willConfirm => {
                if (!willConfirm) throw null;

                if (willConfirm) {
                    setloading(true);
                    if(willConfirm < cant){
                        await materialService.updateMaterialCantService(id_material_fk, cant - willConfirm);
                        await materialService.addMaterialSelectionService(id_material_fk, Number(idGroupResearch), willConfirm);
                        getMaterial();
                        setloading(false);
                        swal({
                            text: `Se ha añadido correctamente a la seleccion de recursos`,
                            icon: "success"
                        });
                    }
                    else{
                        setloading(false);
                        swal({
                            text: `Upsi dopsy`,
                            icon: "error"
                        });
                    }
                }

            })
            .catch(err => {
                swal({
                    text: err,
                    icon: "error"
                });
            });
    }

    return (
        <div>
            {
                loading && <SpinnerOrbits />
            }

            {!loading && materialState.length > 0 ?
                <div>
                    <div className="collection">
                        <a className="collection-item pt35 height100">
                            <span className="badge" data-badge-caption="Materiales disponibles para su selección">
                                {materialState.length}
                            </span>
                            Catalogo de materiales
                        </a>
                    </div>
                    <div className="row mt70">

                        {
                            materialState.map((resource) => (
                                <ItemResource
                                    key={resource.id}
                                    resource={resource}
                                    onDeleteResource={onDeleteResource}
                                    pathEdit={`/ui/resources/UpdateMaterial/${resource.id}`}
                                    selectioResource={selectionMaterial}
                                />
                            ))
                        }
                    </div>
                </div>
                : <center className="mt100">No hay materiales disponibles</center>
            }

            {
                !loading &&
                rol.rol_name === 'teacher' &&
                <div className="fixed-action-btn">
                    <Link
                        to={'/ui/resources/AddMaterial'}
                        className="waves-effect waves-light btn cyan darken-4"
                    >
                        <i className="material-icons right">add</i>Nuevo Material
                    </Link>
                </div>
            }
        </div>
    )
}
