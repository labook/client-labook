import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import swal from 'sweetalert';
import { useForm } from '../../../../hooks/useForm';
import { materialService } from '../../../../utils/services/MaterialService';
import { SpinnerOrbits } from '../../../UI/spinnerOrbits/SpinnerOrbits';
import { ResourcesForm } from '../ResourcesForm';

export const AddMaterial = () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const history = useHistory();
    const [inputValues, handleInputChange, resetForm, handleCheckedChange, handleInputChangeFile, inputValidate] = useForm({
        name: '',
        description: '',
        image: '',
    });

    const isValidInputs = () => {
        const { name, description, image } = inputValues;
        if (name.length === 0 && description.length === 0 && image.length === 0) {
            setError({
                name: 'Nombre requerido',
                description: 'Descripcion requerida',
                image: 'Imagen requerida'
            });
            return false;
        }
        return true;
    }

    const addMaterial = async (newMaterial) => {
        try {
            setLoading(true);
            if (isValidInputs()) {
                await materialService.postMaterialService({ ...newMaterial });
                setLoading(false);
                swal({
                    title: newMaterial.name,
                    text: "Creado correctamente",
                    icon: "success"
                }).then(willacept => {
                    if (willacept) {
                        history.push('/ui/resources/materials');
                        resetForm();
                    };
                });
            }
            setLoading(false);
        }
        catch ({ response }) {
            setLoading(false);
            if (response) {
                swal({
                    title: "Error",
                    text: response.data.message,
                    icon: "error",
                });
            }

        }
    }

    console.log(error)

    return (
        <>
            {
                loading &&
                <SpinnerOrbits />
            }

            <div className="row">
                <div className="col m5 s12 mt30">
                    <Link
                        // to={"/ui/resources/materials"}
                        className="breadcrumb cyan-text darken-4"
                    >
                        Catalogo
                </Link>
                    <Link className="breadcrumb cyan-text darken-4">Crear material</Link>

                </div>
            </div>
            <center><h1>Agregar nuevo material</h1></center>
            <div className="row">
                <ResourcesForm
                    handleInputChange={handleInputChange}
                    inputValues={inputValues}
                    handleInputChangeFile={handleInputChangeFile}
                    addResource={addMaterial}
                    error={error}
                />
            </div>

        </>
    )
}
