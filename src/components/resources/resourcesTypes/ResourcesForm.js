import React, { useState } from 'react';

export const ResourcesForm = ({
    handleInputChange,
    inputValues,
    handleInputChangeFile,
    addResource,
    cas,
    onUpdateResource,
    idUpdate,
    error
}) => {

    return (
        <form className="col s12 m8 offset-m2">
            <div className="row">
                <div className="input-field col s12">
                    <input
                        autoFocus
                        autoComplete="off"
                        id="name"
                        type="text"
                        name="name"
                        onChange={handleInputChange}
                        value={inputValues.name}
                    />
                    <label htmlFor="name">Nombre</label>
                    {error && <p className="red-text fsize12">{error.name}</p>}
                </div>

                {cas &&
                    <div className="input-field col s12">
                        <input
                            autoComplete="off"
                            id="cas"
                            type="number"
                            name="cas"
                            onChange={handleInputChange}
                            value={inputValues.cas}
                        />
                        <label htmlFor="cas">CAS</label>
                        {error && <p className="red-text fsize12">{error.cas}</p>}
                    </div>
                }

                <div className="input-field col s12">
                    <input
                        autoComplete="off"
                        id="description"
                        name="description"
                        type="text"
                        onChange={handleInputChange}
                        value={inputValues.description} />
                    <label htmlFor="description">Descripccion</label>
                    {error && <p className="red-text fsize12">{error.description}</p>}
                </div>

                <div className="input-field col s12">
                    <input
                        autoComplete="off"
                        id="cant"
                        name="cant"
                        type="number"
                        onChange={handleInputChange}
                        value={inputValues.cant} />
                    <label htmlFor="cant">Cantidad de recurso inicial</label>
                    {error && <p className="red-text fsize12">{error.cant}</p>}
                </div>

                <div className="file-field input-field col s12">
                    <div className="btn purple">
                        <span>Subir imagen</span>
                        <input
                            name="image"
                            type="file"
                            onChange={handleInputChangeFile}
                            accept='.jpg, .jpeg, .png'
                        />
                    </div>
                    <div className="file-path-wrapper">
                        <input
                            className="file-path validate"
                            placeholder="Seleccionar imagen del recurso"
                        />
                        {error && <p className="red-text fsize12">{error.image}</p>}
                    </div>
                </div>
                <div className="col s12">
                    <a
                        className="btn cyan darken-3 waves-effect waves-light btn-small right my30"
                        onClick={
                            () => addResource ?
                                addResource(inputValues) :
                                onUpdateResource(idUpdate, inputValues)
                        }
                    >
                        Enviar
                </a>
                </div>
            </div>
        </form>
    )
}
