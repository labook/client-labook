import React, { useEffect } from 'react';
import { Switch, useHistory } from "react-router-dom";
import { RouteWithSubRoutes } from '../../utils/config/RouteWithSubRoutes';
import { isAuthenticated } from '../../utils/services/auth/authService';
import { ResourceNavStudent } from './resourcesTypes/resourceNav/ResourceNavStudent';
import { ResourceNavTeacher } from './resourcesTypes/resourceNav/ResourceNavTeacher';
import './Resources.css';

export const ResourceScreen = ({ routes, researchID, idGroupResearch }) => {

  const { rol } = isAuthenticated();
  const history = useHistory();
  useEffect(() => {
    rol.rol_name === 'teacher' && history.replace('/ui/resources/materials');
  }, []);

  return (
    <>
      <header className={"purple lighten-1 m0"}>
        <div className="nav-content">
          <ul className="tabs tabs-transparent">
            {
              rol.rol_name === 'student' &&
              <ResourceNavStudent
                researchID={researchID}
                idGroupResearch={idGroupResearch}
              />
            }

            {
              rol.rol_name === 'teacher' &&
              <ResourceNavTeacher />
            }

          </ul>
        </div>
      </header>

      <Switch>
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route} />
        ))}
      </Switch>

    </>
  )
}



