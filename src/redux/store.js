import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import rootReducer from './reducers';
import thunk from 'redux-thunk'

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

//Almacenamiento de nuestro estado
const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(logger, thunk))
);

export default store;


