import { BOOLEAN } from '../reducers/darkMode.reducer';
//Creadores de acciones
export const activedDarkMode = () => {
    return {
        type: BOOLEAN
    }
}