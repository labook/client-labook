// Services
import { equipmentService } from "../../utils/services/EquipmentService";

export const FETCH_RESOURCES_REQUEST = 'FETCH_RESOURCES_REQUEST';
export const FETCH_RESOURCES_SUCCESS = 'FETCH_RESOURCES_SUCCESS';
export const FETCH_RESOURCES_ERROR = 'FETCH_RESOURCES_ERROR';

export const getEquipment = () => async (dispatch) => {
    dispatch({ type: FETCH_RESOURCES_REQUEST })
    try {
        const equipments = await equipmentService.getEquipment(1, 5);
        console.log("GetEquipment Actions");
        console.log(equipments);
        dispatch({
            type: FETCH_RESOURCES_SUCCESS,
            payload: {
                resources : equipments.data
            }
        })
    }
    catch (error) {
        dispatch({
            type: FETCH_RESOURCES_ERROR,
            payload: {
                error: error.toString()
            }
        })
    }

}