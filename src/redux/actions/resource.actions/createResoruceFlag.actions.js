import { NEW_RESOURCE_FLAG, NEW_RESOURCE_FLAG_FALSE } from '../../reducers/resource.reducers/createResourceFlag.reducer';
import { SHOW_BUTTON_RESOURCE, DESACTIVE_BUTTON_RESOURCE } from '../../reducers/resource.reducers/showButtonCreateResource.reducer';
import { UPDATE_RESOURCE_FLAG, UPDATE_RESOURCE_FLAG_FALSE } from '../../reducers/resource.reducers/updateResourceFlag.reducer';

//Creadores de acciones
export const activedFlag_createResource = () => {
    return {
        type: NEW_RESOURCE_FLAG
    }
}

export const desactivedFlag_createResource = () => {
    return {
        type: NEW_RESOURCE_FLAG_FALSE
    }
}

export const activedFlag_updateResource = () => {
    return {
        type: UPDATE_RESOURCE_FLAG
    }
}

export const desactivedFlag_updateResource = () => {
    return {
        type: UPDATE_RESOURCE_FLAG_FALSE
    }
}

export const showButton_createResource = () => {
    return {
        type: SHOW_BUTTON_RESOURCE
    }
}

export const desactive_button_createResource = () => {
    return {
        type: DESACTIVE_BUTTON_RESOURCE
    }
}