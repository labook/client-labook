import { UPDATE_NAME_RESOURCE } from '../../reducers/resource.reducers/updateResourceItem.reducer';

export const updateNameResource = (id, name, image, description) => {
    return {
        type: UPDATE_NAME_RESOURCE,
        payload: {
            id,
            name,
            image,
            description
        }
    }
}