import { combineReducers } from 'redux';
import darkMode from './darkMode.reducer';
import user from './user.reducer';
import createResourceFlag from './resource.reducers/createResourceFlag.reducer';
import updateResourceFlag from './resource.reducers/updateResourceFlag.reducer';
import updateResourceItem from './resource.reducers/updateResourceItem.reducer';
import showButtonCreateResource from './resource.reducers/showButtonCreateResource.reducer';

export default combineReducers({ darkMode, user, createResourceFlag, updateResourceFlag, updateResourceItem, showButtonCreateResource });
