import { FETCH_RESOURCES_REQUEST, FETCH_RESOURCES_SUCCESS, FETCH_RESOURCES_ERROR } from '../actions/resources.actions';
const initialState = {
    resources: [],
    isFetching: false,
    error: null
}

function resources(state = initialState, action) {
    switch (action.type) {
        case FETCH_RESOURCES_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case FETCH_RESOURCES_SUCCESS: 
            return{
                ...state,
                isFetching: false,
                resources: action.payload.resources
            }
        case FETCH_RESOURCES_ERROR:
            return {
                ...state,
                isFetching: false,
                error: action.payload.error
            }
        default:
            return state
    }
}

export default resources;