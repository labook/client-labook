export const initialState = false;
export const NEW_RESOURCE_FLAG = 'NEW_RESOURCE_FLAG';
export const NEW_RESOURCE_FLAG_FALSE = 'NEW_RESOURCE_FLAG_FALSE';

function createResourceFlag(state = initialState, action) {
    switch (action.type) {
        case NEW_RESOURCE_FLAG: return !state;
        case NEW_RESOURCE_FLAG_FALSE: return false;
        default:
            return state;
    }

}

export default createResourceFlag;