export const initialState = false;
export const UPDATE_RESOURCE_FLAG = 'UPDATE_RESOURCE_FLAG';
export const UPDATE_RESOURCE_FLAG_FALSE = 'UPDATE_RESOURCE_FLAG_FALSE';


function updateResourceFlag(state = initialState, action) {
    switch (action.type) {
        case UPDATE_RESOURCE_FLAG: return !state;
        case UPDATE_RESOURCE_FLAG_FALSE: return false
        default:
            return state
    }

}

export default updateResourceFlag;