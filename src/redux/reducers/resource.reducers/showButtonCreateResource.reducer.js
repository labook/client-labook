export const SHOW_BUTTON_RESOURCE = 'SHOW_BUTTON_RESOURCE';
export const DESACTIVE_BUTTON_RESOURCE = 'DESACTIVE_BUTTON_RESOURCE';
export const initialState = false;

function showButtonCreateResource(state = initialState, action) {
    switch (action.type) {
        case SHOW_BUTTON_RESOURCE: return !state;
        case DESACTIVE_BUTTON_RESOURCE: return false;
        default:
            return state
    }
}

export default showButtonCreateResource;