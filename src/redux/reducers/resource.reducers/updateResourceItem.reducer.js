const initialState = {
    name: '',
    image: '',
    description: ''
}

export const UPDATE_NAME_RESOURCE = 'UPDATE_NAME_RESOURCE';

function updateResourceItem(state = initialState, action) {
    switch (action.type) {
        case UPDATE_NAME_RESOURCE:
            const { id, name, image, description } = action.payload;
            return {
                ...state,
                id,
                name,
                image,
                description
            }
        default:
            return state
    }

}

export default updateResourceItem;