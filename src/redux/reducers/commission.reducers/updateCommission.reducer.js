import { types } from "../../types/types";

export const updateCommissionReducer = (state = {}, action) => {

    switch (action.type) {
        case types.commissionUpdated:
            return {
                ...state,
            }
        default:
            return state;
    }
}
