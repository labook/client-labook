export const initialState = false;
export const BOOLEAN = 'BOOLEAN';

function darkMode(state = initialState, action) {
    if (action.type === BOOLEAN) {
        return !state;
    }
    return state;
}

export default darkMode;